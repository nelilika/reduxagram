/* Bank Account */
// getMoney();
// putMoney();
// getBalance();
// getCreditLimit();

class User {
  #idPrivate = 1; // приватное свойство создается каждый раз при создании экзепляра класса
  // и не доступно в экземпляре объекта
  idPublic = 1; // публичное свойство создается каждый раз при создании экзепляра класса
  // и доступно в экземпляре объекта === this.idPublic;
  static idStatic = 0; // статическое свойство, доступно как свойство класса === User.idStatic

  constructor(name, birthDay, passport) {
    this.name = name;
    this.birthDay = User.parseBirthDay(birthDay);
    this.passport = passport;
    this.id = ++User.idStatic;
  }

  static parseBirthDay(data) {
    return data.getTime(); // data в милисекундах
  } // статический метод
}

// function UserFn() {}

// UserFn.idStatic = 1;

// bad practice
// User.id = 1;

const user1 = new User('L', new Date(2000, 0, 0), 'RT234567');
const user2 = new User('L', new Date(2000, 0, 0), 'RT234567');
const user3 = new User('L', new Date(2000, 0, 0), 'RT234567');

console.log(user1);
console.log(user2);
console.log(user3);

class BankAccount {
  static id = 0;
  #money = 0;
  #creditLimit = 0;
  #history = [];

  constructor(cardType, currency, userId) {
    this.cardType = cardType;
    this.currency = currency;
    this.userId = userId;
    this.id = ++BankAccount.id;
  }

  // getter - для чтения
  get balance() {
    return this.#money;
  }

  get limit() {
    return this.#creditLimit;
  }

  get history() {
    return this.#history;
  }

  // getBalance() {
  //   return this.#money;
  // }

  // setter - для записи
  set balance(value) {
    this.#money += value;
    this.#addToHistory(`added to wallet: ${value}`);
  }

  set limit(value) {
    this.#creditLimit = value;
    this.#addToHistory('limit was changed');
  }

  // setBalance(value) {
  //   this.#money += value;
  // }

  withdraw(value) {
    if (this.#money < value && this.#money + this.#creditLimit > value) {
      this.#money -= value;
      this.#money -= this.calculatePersent();
    } else if (this.#money + this.#creditLimit > value) {
      this.#money -= value;
      this.#addToHistory(`withdraw: ${value}`);
    } else {
      console.error('Too much money');
      // throw new Error('You are not so rich');
    }
  }

  #addToHistory(message) {
    const history = {
      message,
      date: new Date(),
    };
    this.#history = [...this.#history, history];
  }

  calculatePersent() {
    // 4% комиссия
    return (this.money / 100) * 4;
  }
}

class InternationalBankAccount extends BankAccount {
  constructor(cardType, currency, userId) {
    super(cardType, currency, userId);
  }

  calculatePersent() {
    // 8% комиссия
    return super.calculatePersent() * 2; // (this.money / 100) * 4 * 2
  }
}

const card = new BankAccount('debit', 'UAN', user1.id);
console.log(card);
