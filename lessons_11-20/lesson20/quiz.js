// 1
{
  let result = {}.prototype === {}.__proto__; // false
  let result2 = {}.__proto__ === Object.prototype; // true
  console.log('1: ', result2);
}

// 2
{
  function getName() {}
  let result = getName.prototype === getName.__proto__; // false
  let result2 = getName.__proto__ === Function.prototype; // true
  console.log('2: ', result2);
}

// 3
{
  function getMartingName() {}
  function getJohnName() {}
  let result = getMartingName.prototype === getJohnName.prototype; // false
  console.log('3: ', result);
}

// 4
{
  let age = 25;
  let result = age.prototype === Number.prototype; // false
  let result2 = age.__proto__ === Number.prototype; // true
  console.log('4: ', result2);
}

// 5
{
  class Worker {}
  let result = Worker.__proto__ === Function.prototype; // true
  console.log('5: ', result);
}

// 6
{
  let result = Object.create(null).__proto__ === Object.prototype.__proto__; // false
  let result2 = Object.create(null).__proto__ == Object.prototype.__proto__; // true
  // Object.create(null).__proto__ // undefined
  // Object.prototype.__proto__ // null
  console.log('6: ', result2);
}

// 7
{
  let result = [].__proto__ === Array.prototype; // true
  console.log('7: ', result);
}

// 8
{
  let count = 'qwerty';
  let result = count.__proto__ === Object.getPrototypeOf(count); // true
  let result2 = count.__proto__ === String.prototype; // true
  let result3 = Object.getPrototypeOf(count) === String.prototype; // true
  console.log('8: ', result);
}
