// Прототипы или прототипное наследование

// const obj = {};
// const obj = new Object();
// const

const student = {
  name: 'Arkadii',
  age: 25,
  isStudent: true,
};

console.log(student);

function getName(name) {
  return 'hello ' + name;
}

const anon = function (name) {
  return 'hello ' + name;
};

const arrow = (name) => {
  return 'hello ' + name;
}; // стрелочная функция не имеет конструктора и не может создавать
// экземпляры объектов

console.dir(getName);

const arr = [1, 2, 3];
console.log(arr);

// Прототип [[Prototype]] или (__proto__) есть у ВСЕХ объектов в javaScript

// ссылка на функцию конструктор с помощью которого создается
// экземпляр объекта (через new)

// Два исключения, когда у объекта нет прототипа
// 1) Object.prototype.__proto__
// 2) Object.create(null)

// F.prototype - объект
// constructor является обязательным ключом
// прототип есть только у функций (если это не стрелочная функция)

function Device(display) {
  this.display = display;
}
Device.prototype.getName = function () {
  return 'hello from prototype';
};

Device.prototype.logString = "hello, i'm string from prototype";

const device = new Device('Java');
console.log(device);

// device[[Prototype]] === Device.prototype;
// родителем объекта device является ключ прототип функции-конструктора Device
device.__proto__ === Device.prototype;

Array.prototype.customFn = function () {
  return 'Hello im function from array';
};

Array.prototype.find = function () {
  return 'Hello im find function from array';
};
