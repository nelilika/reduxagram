/*
    turnOn() {
      console.log('turned on');
      isTurnedOn = true;
    },
    turnOf() {
      console.log('turned off');
      isTurnedOn = false;
    },
    getComputerStatus() {
      // замыкание
      return isTurnedOn;
    },
    showApplications() {
      console.log('showed application');
    },
 */

function Device(display) {
  this.display = display;
  this.isTurnedOn = false;
}

Device.prototype.turnOn = function () {
  console.log('turned on');
  this.isTurnedOn = true;
};

Device.prototype.turnOf = function () {
  console.log('turned off');
  this.isTurnedOn = false;
};

function Computer(mouse, keebord, system, display) {
  Device.call(this);
  this.mouse = mouse;
  this.keebord = keebord;
  this.system = system;
  this.display = display;
}

Computer.prototype = Device.prototype;
Computer.prototype.download = function (fileName) {
  console.log('downloaded file: ', fileName);
};

// CLASS
class DeviceClass {
  #isTurnedOn = false;

  constructor(display) {
    this.display = display;
  }

  turnOn() {
    console.log('turned on');
    this.#isTurnedOn = true;
  }

  turnOf() {
    console.log('turned off');
    this.#isTurnedOn = false;
  }
}

class ComputerClass extends DeviceClass {
  constructor(mouse, keebord, system, display) {
    super(display);
    this.mouse = mouse;
    this.keebord = keebord;
    this.system = system;
  }

  download(fileName) {
    console.log('downloaded file: ', fileName);
  }

  turnOn() {
    super.turnOn();
    console.log('turned on computer');
  }
}

const computerFake = Computer('Apple', 'Apple', 'Apple', 'Apple'); // вызов функции
const computer1 = new Computer('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

const computer2 = new ComputerClass('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

console.log(computer2);

// for ... in проходится по ключам объекта и так же по ключам всех
// его прототипов (родителей)
for (let key in computer1) {
  // console.log(key);
}

for (let key in computer1) {
  if (computer1.hasOwnProperty(key)) {
    // console.log(key);
  }
}

// best practice
for (let key of Object.keys(computer1)) {
  // console.log(key);
  // computer1[key]
}

// instanceof наследуется ли заданный объект от функции конструктора
computer1 instanceof Device; // true
