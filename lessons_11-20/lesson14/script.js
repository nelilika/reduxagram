// DOM

console.log(document.head);
console.log(document.body);

// возвращает МАССИВ кнопок
document.getElementsByTagName('button');

// возвращает МАССИВ элементов с классом text
document.getElementsByClassName('text');

// возвращает первый элемент по id
document.getElementById('header');

// highly recomended
document.querySelector('ul li:nth-child(3)');
document.querySelectorAll('ul li:nth-child(3)');

const li = document.querySelector('ul li:nth-child(3)');

// Изменение значения обьекта
li.innerHTML = `<div><strong>Hello World</strong></div>`;

li.innerText = `<div><strong>Hello World</strong></div>`;

li.textContent = 12;

// работа с атрибутами
const ul = document.querySelector('ul');
ul.getAttribute('class'); // 'list'
ul.getAttribute('id'); //'header'
ul.getAttribute('data'); //null

ul.setAttribute('data-context', 'list of numbers');

ul.getAttribute('data-context'); // list of numbers

// Создание нового элемента

document.createElement('button');
const appendLi = document.createElement('li');

appendLi.textContent = 'append Li';

// const textLi = document.createTextNode('new node Li');
// appendLi.append(textLi);
ul.append(appendLi); // добавить внутрь UL в конец

const prependLi = document.createElement('li');
prependLi.textContent = 'prepend Li';
ul.prepend(prependLi); // добавить внутрь UL в начало

const beforeLi = document.createElement('p');
beforeLi.textContent = 'before p';
ul.before(beforeLi); // добавить перед UL

const afterLi = document.createElement('p');
afterLi.textContent = 'after p';
ul.after(afterLi); // добавить после UL

// Добавление элементов в цикле
let i = 0;
while (i < 5) {
  const li = document.createElement('li');
  li.innerHTML = `<i>Item ${i}</i>`;
  li.setAttribute('data-item', i);
  ul.append(li);
  i++;
}

// Работа с классами
const p = document.querySelector('p.text');

// p.className = 'open' // перезапишет предыдущие классы

p.classList.toggle('open'); // добавить если не существует, удалить если существует

p.classList.add('open');
p.classList.remove('open');
p.classList.contains('open'); // проверить на существование

// Как работать со стилями
const button = document.querySelector('button');
// button.style.display = 'none';
button.style.position = 'absolute';
let topButon = 0;

// setInterval(() => {
//   topButon = topButon + 10;
//   button.style.top = `${topButon}px`;
// }, 500);

// button.onclick = function () {
//   console.log('Hello my first click');
// };

button.addEventListener('click', function (event) {
  console.log('Hello my first click');
});

function onclick(event) {
  console.log(event);
  console.log('Hello my first click 2');
}

button.addEventListener('click', onclick);
