// WebStorages
// Window

// Local Storage
// Session Storage

// localStorage.getItem('key'); - Получить значение коллекции по ключу key
// localStorage.setItem('test', 'Hello from code'); - Задать ключ test со значением Hello from code
// localStorage.removeItem('test'); - Удалить ключ test и его значение

// localStorage.test - Получить значение коллекции по ключу test

let i = 5;

// localStorage.getItem('string').includes('qwerty');

// localStorage работает ТОЛЬКО со строками
// чтобы добавить массив/объект используем JSON

const student = {
  name: 'Adam',
  marks: {
    math: 12,
    english: 5,
  },
  log() {
    console.log('test log');
  },
};

localStorage.setItem('student1', student);
localStorage.setItem('student2', JSON.stringify(student)); //

localStorage.getItem('student1'); // [object Object]
localStorage.getItem('student2'); // '{"name":"Adam","marks":{"math":12,"english":5}}'

// Window

// history.back() history.forward() - назад/вперед на страницу
// open('url') - открыть любой url
// location.href - доступ к url сайта
// location.reload() - перегрузить страницу
// location.replace(url) - редикрет на другую страницу

function success(pos) {
  const crd = pos.coords;

  console.log('Your current position is:');
  console.log(`Latitude : ${crd.latitude}`);
  console.log(`Longitude: ${crd.longitude}`);
  console.log(`More or less ${crd.accuracy} meters.`);
}

function error(err) {
  console.error(err);
  console.warn(`ERROR(${err.code}): ${err.message}`);
}

navigator.geolocation.getCurrentPosition(success, error);
