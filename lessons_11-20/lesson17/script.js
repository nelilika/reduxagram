/************** 
  Регулярные выражения (Regular expression)
**************/

/* Создание регулярного выражения */
const regExp1 = /word/;
// console.log(regExp1);

const regExp2 = new RegExp('word');
// console.log(regExp2);

/* Методы регулярных выражений */

// /regexp/.test(string) -> boolean
// console.log(/word/.test('Hello word word word word'));

/* Методы строк, которые МОГУТ работать с регулярными выражениями */
// string.match(/regexp/) -> array | null - возращает массив с найденной строкой(-ами если включен флаг g)
// console.log('Hello'.match(/word/g));

/* Флаги */

// i - регистронечуствительный поиск
// console.log(/word/i.test('Hello worD'));

// g - глобальный поиск
// console.log('parallelepiped'.match(/le/g)); // ['le', 'le'];
// console.log('parallelepiped'.match(/le/)); // [ 'le', index: 5, input: 'parallelepiped', groups: undefined ]

// m - многоуровневый поиск
// console.log('Hello word,\nword,\nworder,\nunword'.match(/word/m));

// s - ?

/* Символьные классы */

// \d - объединяет цифры - 0-9

// const result = '+38 (063) 123-45-67'.match(/\d/g);
// console.log(+result.join('')); // 380631234567

// \w - обЪединяет символы, цифры и _

// const result = '+38_(063) 123-45-67y'.match(/\w/g);
// console.log(result.join('')); // 38_0631234567y

// \s - объединяет все пробельные символы -> ' ', '\n', '\t',

// const result = '+38 (063) 123\t-45-67y\n'.match(/\s/g);
// console.log(result); // [ ' ', ' ', '\t', '\n' ]

// \D     ->
// \W     -> ВСЕ КРОМЕ
// \S     ->

// const result = '+38 (063) 123\t-45-67y\n'.match(/\W/g);
// console.log(result); // [ ' ', ' ', '\t', '\n' ]

/* Символы */

// ^ - начало строки
// console.log(/^word/.test('Hello word word word word'));

// $ - конец строки
// console.log(/word$/.test('Hello word word word word'));

// . - абсолютно любой символ
// console.log(/\.ua$/.test('googlua'));

// | - ИЛИ
console.log(/hello|goobye/gi.test('Goobye, world'));
console.log(/hello|goobye/gi.test('Hello, world'));

// console.log(
//   /^word/m.test(`
// hello
// word
// word
// word`)
// ); // true

// console.log(/^Hello word$/.test('Hello word'));

// \b - граница слова
// console.log(/\bhello\b/i.test('b?Hello^&Marina,Anna,Ola! b'));

/* Квантификаторы */

// +, {1,} - 1 или более символов
// console.log('564.34.2012'.match(/\d+/));

// {1,2} - 1 или 2 символа
// console.log('564.34.2012'.match(/\b\d{1,2}\b/));
// {5} - 5 символов
// console.log('564.34.2012'.match(/\b\d{5}\b/));
// ?, {0, 1} - 0 или 1
// console.log('https://google.com'.match(/^https?:\/\//g));
// *, {0,} - 0 или более символов
// console.log('http://google.com'.match(/^https*:\/\//g));

// console.log(/hello+/.test('helloooo'));

/* Диапазоны значений */
// [] - квадратные скобочки отвечают за диапазон

// console.log("Hello_, I'm 18 years old! YHUU".match(/[a-z']+/gi)); // [ 'Hello', 'I'm', 'years', 'old', 'YHUU' ]
// console.log("Hello_, I'm 18 years old! YHUU".match(/[0-7]+/gi)); // [1]
// console.log("Hello_, I'm 108 years old! YHUU".match(/[0 7]+/gi)); // [ ' ', ' ', '0', ' ', ' ', ' ' ]

// Replace()

let result = 'Hello, world!'.replace('world', 'Lily');
// console.log(result);

result =
  '     aliufhiwue w;oeifjoie   asflisjfies      aiuefusahief   '.replace(
    /\s+/g,
    ' '
  );
// console.log(result);

result = '12 23 45 566 8734 2 10 52 25 4'.replace(/[4-7]/g, '*');
// console.log(result);

result = '12 23 45 566 8734 2 10 52 25 4'.replace(
  /\b[4-7]{1,2}\b/g,
  (subStr, index, str) => subStr ** 2
);
// console.log(result);
