/*
 - как дебажить код (debug)
 - setTimeout(), setInterval()
*/


function calculateValue(a,b) {
  // debugger;
  console.log('a', a);
  return a + b;
}

let sum = 0;
for(let i=0; i<= 10; i++) {
  sum += i;
}
// console.log(sum);

// console.log('result:', calculateValue(1,2));
// console.log('result:', calculateValue(3,4));

function counterProtector() {
  let count = 0;
  return function countFn() {
    return ++count;
  }
}
const counterGlobal = counterProtector();

// console.log(counterGlobal()); 
// console.log(counterGlobal());
// console.log(counterGlobal());

// setTimeout(), setInterval()
// функции - планирования

function getName() {
  console.log('hello world immediately');
}

getName()

const timerId = setInterval(getName, 1000);
console.log(timerId);

setTimeout(function() {
  clearInterval(timerId);
}, 5000);