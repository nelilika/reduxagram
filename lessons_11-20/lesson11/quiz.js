alert(true) + [] + parseFloat('$13'); // 'undefinedNaN'
Number('23') - Number(true) - Number(null) - undefined // NaN
!{} + '5' - false || 1 // 1
![] - '5' + (false || 1) // -4
'false' && 'true' + {} - [] // NaN
{} + {} + {} + [] + [{}] + [{},{}] // 'NaN[object Object][object Object][object Object],[object Object]'
'hello' + ((+'50' / (9 + !!true)) - ([] + 5)) // 'hello0'
!!'{}' + !!{} - !!'[]' - !![] // 0
([] > {}) - ([] < {}) // -1
('5' + +[5] + true * 5) / (!![] * 5) // 111
((NaN == NaN) > -1) + +'16' - [] // 17
false + {} - [] + [{}] // NaN[object object]
1 + '2' + true + false + null + undefined + {} + [] // '12truefalsenullundefined[object object]'
[] + {} === {} + [] // true
