// OOP

// Инкапсуляция, Полиморфизм и Наследование
// Абстракция

function Device(display) {
  let isTurnedOn = false;

  return {
    display,
    turnOn() {
      console.log('turned on');
      isTurnedOn = true;
    },
    turnOf() {
      console.log('turned off');
      isTurnedOn = false;
    },
    getComputerStatus() {
      // замыкание
      return isTurnedOn;
    },
    showApplications() {
      console.log('showed application');
    },
  };
}

// Компьютер наследуется от Девайса
function Computer(mouse, keebord, system, display) {
  const baseEquipment = Device(display);

  return {
    ...baseEquipment,
    // cвойства
    mouse,
    keebord,
    system,

    // методы
    download(fileName) {
      console.log('downloaded file: ', fileName);
    },
    run(program) {
      console.log(`${program} is running`);
    },
    showApplications() {
      console.log('showed application after you clicked My PC');
    },
  };
}

// Телефон наследуется от Девайса
function Phone(display) {
  const baseEquipment = Device(display);

  return {
    ...baseEquipment,
    // cвойства

    // методы
    takePhone() {
      console.log('took phote: ');
    },
    run(program) {
      console.log(`${program} is running`);
    },
    showApplications() {
      console.log('showed application on the main screen');
    },
  };
}

// эксземпляр класса
const computer1 = Computer('HP', 'HP', 'HP', 'HP');
const computer2 = Computer('Samsung', 'Samsung', 'Samsung', 'Samsung');
const computer3 = Computer('Apple', 'Apple', 'Apple', 'Apple');
const computer4 = Computer('IBM', 'IBM', 'IBM', 'IBM');

const phone1 = Phone('Japanese');

const computers = [computer1, computer2, computer3, computer4];
// computer1.turnOn();
// computer3.turnOn();

// for (let comp of computers) {
//   console.log(comp.mouse);
//   console.log(comp.getComputerStatus());
// }

const devices = [...computers, phone1];

for (let device of devices) {
  console.log(device.display);
  device.showApplications();
}
