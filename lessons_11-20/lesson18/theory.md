OOP

Инкапсуляция, Полиморфизм и Наследование
Абстракция

Компьютер

{
Монитор
Мышка
Клавиатура
Системный блок

включить()
выключить()
загрузить документ()
запустить игру()
}

Инкапсуляция / Сокрытие

Наследование
Техника (
Дисплей,
включить()
выключить()
)

Все это наследуется от Техники

- компьютер (Дисплей, Мышка, Клавиатура, Системный блок)
- телефон (Дисплей)
- ноутбук (Дисплей, Клавиатура)

Полиморфизм
