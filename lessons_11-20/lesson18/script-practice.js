// функция - конструктор
// классы с es6
// Цель: создание объектов (эксземпляров класса)

function Device(display) {
  this.display = display;
}

function Computer(mouse, keebord, system, display) {
  Device.call(this); // наследование
  this.mouse = mouse;
  this.keebord = keebord;
  this.system = system;
  this.display = display;
}

class DeviceClass {
  constructor(display) {
    // this = {}
    this.display = display;
  }
}
new DeviceClass('Japanisse');

class ComputerClass extends DeviceClass {
  constructor(mouse, keebord, system, display) {
    // this НЕ СОЗДАЕТСЯ а должен взяться из родительского класса
    super(display);
    this.mouse = mouse;
    this.keebord = keebord;
    this.system = system;
  }
}

const computer1 = Computer('Apple', 'Apple', 'Apple', 'Apple'); // вызов функции
const computer2 = new Computer('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

const computer3 = new ComputerClass('Apple', 'Apple', 'Apple', 'Apple'); // создание эксземпляра класса

console.log(computer3);

// что делает new

// функция всегда возвращает объект
// создает свой контекст this -> {}
