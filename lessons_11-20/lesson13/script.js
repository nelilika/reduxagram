// Task 1;

{
  const object = {
    message: 'Hello, World!',
    getMessage() {
      const message = 'Hello, Earth!';
      return this.message;
    },
  };
  console.log(object.getMessage()); // Hello, World
}

// Task 2
{
  const object = {
    message: 'Hello, World!',
  };
  function logMessage() {
    // this is window
    console.log(this.message); // "Hello, World!"
  }

  // bind / call / apply
  logMessage.call(object); // this is { message: 'Hello, World!' }
}

// Task 3
{
  const object = {
    message: 'Hello, World!',
    logMessage() {
      console.log(this.message); // undefined
    },
  };
  setTimeout(object.logMessage, 1000);
}

// Task 4
{
  const object = {
    who: 'World',
    greet: function () {
      console.log(this); // object
      return `Hello, ${this.who}!`;
    },
    farewell: () => {
      console.log(this); // window
      return `Goodbye, ${this.who}!`;
    },
  };
  console.log(object.greet()); // What is logged?
  console.log(object.farewell()); // What is logged?
}

// Task 5
{
  var length = 4;

  function callback() {
    console.log(this); // window
    console.log(this.length); // 4
  }

  const object = {
    length: 5,
    method(callback) {
      console.log(this);
      callback.call(this);
    },
  };
  object.method(callback, 1, 2);
}

// Task 6
{
  let myName = 'Antonina';

  greetMeWithDelay();

  myName = 'Kalush';

  function greetMeWithDelay() {
    setTimeout(function () {
      console.log('Hi ' + myName);
    }, 1500);
  }
}
