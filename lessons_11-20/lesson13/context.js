const weather = {
  min: -50,
  max: 48,
  init: 'celsius',
  getRange() {
    console.log(this);
    return this.max - this.min;
  },
  logDegress() {
    console.log(`Max: ${this.max}; Min: ${this.min}`);
  },
  setDegress(newMin) {
    this.min = newMin;
    this.max = Math.random() * 10;
  },
  parseDegress() {
    return `${this.min} degrees`;
  },
  trickLogRange() {
    console.log(this);

    setTimeout(function () {
      console.log(this);
      console.log(this.max - this.min);
    }, 2000);
  },
  bindLogRange() {
    function logDegress() {
      console.log(this);
      console.log(this.max - this.min);
    }

    setTimeout(logDegress.bind(this), 2000);
  },
  arrowLogRange() {
    setTimeout(() => {
      console.log(this);
      console.log(this.max - this.min);
    }, 2000);
  },
};

function getRange() {
  return this.max - this.min;
}
console.log(getRange());

// function () {
//   console.log(this);
//   console.log(this.max - this.min);
// }
