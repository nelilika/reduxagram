/**
 * reduce()
 * потеря контекста
 * практика
 */

// Метод массива reduce()

// .method((item, index, arr) => {})
// .method(number/string)
let sum = 0;
const arr = new Array(10).fill(null).map((item, i) => i + 1);

// bad practice
for (let i = 0; i < arr.length; i++) {
  sum += arr[i];
}

console.log(arr);

// Начальное значение acc (первый параметр функции калбек метода reduce)
// arr.reduce(fn) -> acc === arr[0] на нулевой итерации
// arr.reduce(fn, '') -> acc === '' на нулевой итерации

const result = arr.reduce((acc, item, i, arr) => {
  console.log('acc', acc);
  console.log('item', item);
  console.log('iteration', i);
  return item; // return записывает в acc на следующую итерацию
}, 'hello');

// result - это то что вернет return на последней итерации
console.log(result);

// good practice
{
  const result = arr.reduce((acc, item, i, arr) => {
    console.log('acc', acc);
    console.log('item', item);
    console.log('iteration', i);
    return acc + Math.pow(item, i);
  }, 0);

  console.log(result);
}
// filter Array
{
  const result = arr.reduce((acc, item, i, arr) => {
    if (i % 2) {
      return [...acc, item];
    }
    return acc;
  }, []);

  console.log(result);
}
// map Array
{
  const result = arr.reduce((acc, item, i, arr) => {
    return [...acc, item.toString()];
  }, []);

  console.log(result);
}

{
  const result = arr
    .filter((item) => !(item % 2))
    .map((item) => item.toString());
  console.log(result);
}
// ===
{
  const result = arr.reduce((acc, item, i, arr) => {
    if (i % 2) {
      return [...acc, item.toString()];
    }
    return acc;
  }, []);
  console.log(result);
}

// spread оператор
{
  const arr = [1, 2, 3, 4];
  const newArr = [...arr]; // [1,2,3,4];

  const newArr1 = [...arr, 5, 6]; // [1,2,3,4,5,6];

  const arr2 = [5, 6, 7];
  const newArr2 = [...arr, ...arr2]; //[1,2,3,4,5,6,7];
  const newArr3 = [...arr, arr2]; //[1, 2, 3, 4, [5,6,7]];
}

{
  const names = `
    Scarlett Mercado
    Grayson Rowley
    Iris Vega
    Hester Corona
    Regan Coulson
    Hunter Hernandez
    Maison Currie
    Lydia Wormald
    Rebecca Byrne
    Zoha Christie
    Ava-Grace Edmonds
    Baran Lutz
    Kaycee Dunlap
    Patricia Salinas
    Zacharia Ball
`;
  const courses = ['Front End Basic', 'Front End Pro', 'Pyton', 'UX/UI'];

  const data = names
    .trim()
    .split('\n')
    .map((item) => {
      const [firstName, lastName] = item.trim().split(' ');
      return {
        firstName,
        lastName,
        dateOfBirth: new Date(),
        course: courses[getRandomIntInclusive(0, courses.length - 1)],
      };
    });

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  console.log(data);

  // {
  //  course: ['', ''],
  // }

  const uniqueCourse = uniqueCourse.filter(
    (item, i, self) => self.indexOf(item) === i
  );
  console.log(uniqueCourse);
  const result = data.reduce((acc, item, i, arr) => {
    return {
      ...acc,
      [item.course]: arr
        .filter((student) => student.course === item.course)
        .map((item) => item.firstName + ' ' + item.lastName),
    };
  }, {});

  console.log(result);
}
