const button = document.querySelector('.btn');

button.addEventListener('click', function() {
  console.log('You click the button');
}, {
  once: true,
  capture: false,
  passive: false,
});

const li = document.querySelector('#header li:last-child');
const ul = document.querySelector('ul#header');
const divWrapper = document.querySelector('.wrapper');

li.addEventListener('click', function(event) {
  event.stopImmediatePropagation();
  console.log('Clicked on Li');
  // console.log(event);
  // console.log(this);
  // console.log(event.target);
});
li.addEventListener('click', function(event) {
  event.stopPropagation();
  console.log('Clicked on Li second');
  // console.log(event);
  // console.log(this);
  // console.log(event.target);
});

ul.addEventListener('click', function(event) {
  console.log('Clicked on UL');
  // console.dir(event.target);
}/*, {
  capture: true,
}*/);

const newLi = document.createElement('li');
newLi.textContent = 42;
ul.append(newLi);

divWrapper.addEventListener('click', function(event) {
  console.log('Clicked on Div Wrapper');
  // console.log(event);
  // console.log(this.target);
  // console.log(event.target);
});

// const form = document.querySelector('#loginForm');

document.forms.loginForm.addEventListener('submit', function(event) {
  event.preventDefault();
  console.log(event);
  console.log(event.target.username.value);
  console.log(event.target.password.value);
});

document.forms.loginForm.username.addEventListener('input', function(event) {
  console.log(event.target.value);
})