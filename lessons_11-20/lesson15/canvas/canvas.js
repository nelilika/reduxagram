const canvas = document.getElementById('canvas');
const input = document.querySelector('#color');
const ctx = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const squareWidth = 10;

let isMouseDown = false;

canvas.addEventListener('mousedown', function(event) {
  isMouseDown = true;
  ctx.fillStyle = input.value;
  ctx.strokeStyle = input.value;
});

canvas.addEventListener('mouseup', function(event) {
  ctx.beginPath();
  isMouseDown = false;
});


canvas.addEventListener('mousemove', function(event) {
  if (isMouseDown) {
    ctx.lineWidth = squareWidth;
    ctx.lineTo(event.clientX, event.clientY);
    ctx.stroke();
    ctx.moveTo(event.clientX, event.clientY);
  }
});

// input.addEventListener('change', function(event) {
//   console.log(event.target.value);
// });

function getRandomColor() {
  return `rgb(${getRandomIntInclusive(0, 255)},${getRandomIntInclusive(0, 255)},${getRandomIntInclusive(0, 255)})`;
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}