'use strict';
// Почему не стоит использовать метод forEach
const arr = new Array(10000).fill(null);

// 15
arr.forEach((item) => {
  if (item == 2) {
    console.log(item);
  }
}); // .find // 10000
arr.find((item) => item === 15); // 15

// 15
arr.forEach((item) => {
  if (Number.isInteger(item)) {
    console.log(item);
  }
}); // filter -
arr.filter((item) => item === 15); // 15

arr.forEach((item, i) => {
  if (i == 2) {
    // console.log(item);
  }
}); // .findIndex

// Контект, this

/*
 * Работа var, let/const
 * function () {} - лексическая область видимости
 * замыкание
 */
const firstName = 'Vasya';
function getName() {
  console.log(this);
  return `Hello ${firstName}`;
}

getName('Andrii');

function studentInfo() {
  getName('Lili');
}

console.log(this);

const employee = {
  id: 10,
  name: 'Marlene',
  surname: 'Rogers',
  salary: 5260,
  workExperience: 49,
  isPrivileges: true,
  gender: 'female',
};

const getFullInfo = function (country) {
  return `
    name: ${this.name} ${this.surname}
    totalSum: ${this.workExperience * this.salary},
    gender: ${this.gender === 'male' ? '\uD83D\uDC68' : '\uD83D\uDC69'},
    country: ${country},
  `;
};

// console.log(getFullInfo());

const employee1 = { ...employee, name: 'Uta', surname: 'Atu' };
// console.log(employee1.getFullInfo());

// JSON.parse(JSON.stringify()) - не копирует функции (!)

// Call/Apply, Bind

const student = {
  name: 'studentName',
  surname: 'studentSurname',
  workExperience: 0,
  salary: 0,
  gender: 'male',
};

// Метод Bind - создает новую функцию! с новым контекстом,
// который передается в ее аргумент
// bind фукуцию можно создать только 1 раз
const newGetFullInfo = getFullInfo.bind(student, ['New York']);

const newFn = newGetFullInfo.bind(employee, ['Poland']);
console.log(newFn()); // не будет никаких изменений

const employeeGetFullInfo = getFullInfo.bind(employee);

// console.log(employeeGetFullInfo());

// Метод Call/Apply

function logInfo() {
  console.log(getFullInfo.apply(student, ['USA']));
  console.log(getFullInfo.call(employee, 'Ukraine'));
}

logInfo();

// Math.max(1,2,3,18,5,6); -> 18

// Как использовать Math.max c массивами

Math.max.apply(null, [1, 2, 3, 18, 5, 6]);

Math.max(...[1, 2, 3, 18, 5, 6]);

{
  const arr = [1, 2, 3];
  const arr1 = [...arr];
}

// setTimeout(), setInterval();
// функция планирования
// var i = undefined;
for (let i = 0; i < 5; i++) {
  setTimeout(function () {
    // console.log(i);
  }, i * 1000);
}

// i = 0
// function () {
//  console.log(this);
//   console.log(i);
// }
// console.log('Result', i);

// call stack работает про правилу FILO
// first input last output

{
  console.log(1);

  setTimeout(function () {
    console.log(2);
  }, 0);

  setTimeout(function () {
    console.log(3);
  }, 0);

  console.log(4);
} // 1 4 3 2
