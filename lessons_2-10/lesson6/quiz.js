!!1 // true
!!2 // true

!true // false
!false // true

!true === false // true
1 !== 2 // true

!(1 === 1) // false
!(1 !== 1) // true

!0 // true
!!0 // false

2 || 1 // 2
2 || 0 // 2

2 && 1 // 1
1 && 2 // 2

0 && 2 // 0
0 || 1 || 2 // 1

0 && 1 && 2 // 0
2 || 1 || 0 // 2

2 && 1 && 0 // 0 
null || 2 // 2

undefined && 1 // undefined
(undefined || 2) && (3 || 0) // 3

(2 && 1) || (null && 0) // 1
(2 > 1) && "greater" // "greater"

(2 < 1) && null // false
null && (2 < 1) // null

("" || 2) && (3 || "3.5") || (4 && 5) // 3
(-1 + 1) && "zero" // 0

"-1" + 1 && "oups" // "oups"
(true + "") === "true" // true

{
  let num;
  4 && 5 == '5' && ++num || !0
} // true
true + false // 1

12 / "6" // 2
"number" + 15 + 3 // 'number153'

15 + 3 + "number" // '18number'
"foo" + + "bar" // 'fooNaN'

'true' == true // false
false == 'false' // false

null == '' // false
!!"false" == !!"true" // true