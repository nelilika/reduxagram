/*
  1) Напишіть програму, яка рахує в якій чверті
  знаходиться вказане користувачем число.
  Результат вивести в консоль.

    Якщо number === 48, то виведе 4
    Якщо number === 150, то виведе 0 or Error
    Якщо number === 15, то виведе 2
    Якщо number === 59, то виведе 4
*/

/* 
  2) Напишіть програму, яка питає у користувача число
  і виводить в консоль наступну інформацію про нього:
  - додатнє воно чи від'ємне;
  - скільки цифр в цьому числі;
  - якщо число додатнє, виведіть суму його цифр

    Якщо number === 0, то виведе '0, lenght: 1'
    Якщо number === 100500, то виведе 'positive, length: 6, sum: 6'
    Если number === -50, то виведе 'negative, length: 2'
*/

/*
  3) Відомо, что подорож на Мальдиви коштує 3000$, а купити нові AirPods - 300$.
  Напишіть програму, яка питає у користувача число (в $) та
  виводить в консоль інформацію, що він за ці гроші може купити.

    Якщо money === 200$, то виведе 'You can't do anything. I'm sorry :(';
    Якщо money === 300$, то виведе 'You can buy only AirPods';
    Якщо money === 3200.50$, то виведе 'You can go on vacation or buy AirPods! What are you waiting for?';
    Якщо money === 4300.53, то виведе 'You have enough money for everything. WOW!'
*/

/*
  4) Напишіть програму, яка питає у користувача число,
  виводить на экран всі числа від 1 до цього числа 
  та возводит в степінь 2 кожне ПАРНЕ його число

    Якщо number === 5, то виведе '1 4 3 16 5'
*/

/*
  5) Напишіть програму, яка питає у користувача число
  від 1 до 9 і виводить таблицу множення для цього числа.

    Якщо number === 5, то виведе
      1 x 5 = 5;
      2 x 5 = 10;
      3 x 5 = 15;
      ...
      9 x 5 = 45;

  upd: Виведіть всю таблицу множення.
*/

{
  let number /*= +prompt('Enter your number')*/;
  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    for(let i = 1; i <= 9; i++) {
      const result = `${i} x ${number} = ${i * number}`;
      // console.log(result);
    }
  }
}
{
  for(let i = 1; i <= 9; i++) {
    for (let j = 1; j <= 9; j++) {
      const result = `${i} x ${j} = ${i * j}`;
      // console.log(result);
    }
  }
}

/*
  6) Напишіть програму, яка питає у користувача символ та число
  і виводить цей символ послідовно, збільшуючи кожен раз на 1, поки 
  кількість символів в рядку не буде дорівнювати цьому числу.

    Якщо sybmol === #, number === 5, то виведе
    #
    ##
    ###
    ####
    #####

    Якщо sybmol === @, number === 6, то виведе
    @
    @@
    @@@
    @@@@
    @@@@@
    @@@@@@
*/

/*
  7) Напишіть цикл, який заповнює строку value числами від 1000 до 2000
  і додайте до кожного числа символи '&#'. Результат первірте в браузері,
  запустив index.html файл.

    Формат відповіді:
    console.log(value); // &#1000 &#1001 &#1002 ... &#1999 &#2000

  upd: Перевірте також код для значень від 7000 до 10000
*/

let result = document.getElementById('result');
let value = '';

// your code should be here

result.innerHTML = value;

/*
  8) Напишіть програму, яка питає у користувача число і повертає його
  в перевернутому виді. Програма повина працювати і для додатніх і для
  від'ємних чисел.

    Якщо number === 5678, то виведе 8765
    Якщо number === 8, то виведе 8
    Якщо number === 0, то виведе 0
    Якщо number === -98463, то виведе -36489
*/

{
  let number = +prompt('Enter your number');

  // str.chartAt(0)

  if (isNaN(number) || !Number.isInteger(number)) {
    console.error('Enter not a integer number');
  } else {
    const numberAsStr = number.toString();
    // '5678' index0 === value5, index1 === 6, index2 = 7, index3 = 8
    // '-123' index0 === value-

    let result = '';
    const lastIndex = 0;

    if (number >= 0) {
      for (let i = numberAsStr.length - 1; i >= lastIndex; i--) {
        result += numberAsStr.charAt(i);
      }
    } else {
      result = '-';
      for (let i = numberAsStr.length - 1; i >= lastIndex + 1; i--) {
        result += numberAsStr.charAt(i);
      }
    }

    console.log(+result);
  }
}

/*
  9) Напишіть програму, яка питає у користувача число і виводить на
  экран ряд Фібоначі, який має стільки чисел, скільки запросив користувач.

  upd: Вирішити цю задачу через цикл з порожнім тілом.
*/

{
  let number /*= +prompt('Enter your number')*/;

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let first = 1;
    let second = 1;
    let third = 0;
    let fibonachi = `${first} ${second}`;
  
    for (let i = 0; i <= number; i++) {
      third = first + second;
      first = second;
      second = third;
      fibonachi += ` ${third}`;
    }

    console.log(fibonachi);
  }
}
{
  let number /*= +prompt('Enter your number')*/;

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let fibonachi = '1 1';
  
    for (
      let i = 0,
          first = 1,
          second = 1,
          third = 1;

      i <= number;

      i++,

      third = first + second,
      first = second,
      second = third,
      fibonachi += ` ${third}`
    ) { }

    console.log(fibonachi);
  }
}

/*
  10) Напишіть програму, яка питає у користувача число (number) і підраховує 
  максимальне число n ітерацій за формулою 1 + 2 + ... + n <= number.

    Якщо number === 5, то виведе 2 // 1 + 2 + 3 <= 5
    Якщо number === 8, то виведе 3
*/

{
  let number /*= +prompt('Enter your number')*/;

  if (isNaN(number) || !Number.isInteger(number) || number <= 0) {
    console.error('Enter not a integer number');
  } else {
    let i = 1;
    let sum = 1;
    while (sum <= number) { // 1 <= 8 -> 3 <= 8 -> 6 <= 8
      sum += ++i; // 1 + (2) -> 3 + (3) -> 6 + (4)
    } 

    console.log(i--);
  }
}
