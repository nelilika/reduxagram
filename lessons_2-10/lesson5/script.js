// Конструкция Switch

let colour = 'Yellow';

switch (colour) {
  case 'red':
    console.log('Stay here!');
    break;
  case 'yellow':
    console.log('Wait for it');
    break;
  default: 
    console.log('Now, you go');
    break;
}

if (colour === 'red') {
  console.log('Stay here!');
} else if (colour === 'yellow') {
  console.log('Wait for it');
} else {
  console.log('Now, you go');
}

/** */
const number = 14;
switch (number) {
  case '14': // ===
    console.log('String 14');
    break;
  case 14: // ===
    console.log('Number 14');
    break;
  default: 
    console.log('Not a 14 number');
    break;
} // 'Number 14'

/** */
const color = 'black';
const someCase = 'white';

switch (color) {
  case 'black':
  case someCase:
  case 'grey':
    console.log('color is monochrome');
    break;
  default: 
    console.log('it\'s simple color');
    break;
}

if (color === 'black' || color === 'white' || color === 'grey') {
  console.log('color is monochrome');
} else {
  console.log('it\'s simple color');
}

// Цикл while

// let userNumber = +prompt(); // 10  1 + 2 + 3 + 4 + ... + 10 = ...
let userNumber = 10;

if (isNaN(userNumber)) {
  console.error('Error');
} else {
  let sum = 0; // 1 3 6 10
  let i = 1; // 2 3 4 5
  while (i <= userNumber) {
    sum += i;
    i++;
  }
  console.log(sum);
}

// Цикл For

let userEvenNumber = +prompt(); // 10  2 + 4 + 6 + 8 + 10 = ...

if (isNaN(userEvenNumber)) {
  console.error('Error');
} else {
  let sum = 0;
  for (let i = 1; i <= userEvenNumber; i++) {
    if (i % 2 === 0) { 
      sum += i;
    }
  }

  console.log(sum);
}

// вариант 2
if (isNaN(userEvenNumber)) {
  console.error('Error');
} else {
  let sum = 0;
  for (let i = 2; i <= userEvenNumber; i = i + 2) {
    sum += i;
  }

  console.log(sum);
}

// бесконечный цикл
// while (true) { }
// do { } while (true)
// for (;;) { }

// модернизация цикла for
{
  let sum = 0;
  for (let i = 1; i <= 8; i++) {
    sum += i;
  } // или
  console.log(sum);
}

{
  let i = 1;
  let sum = 0;
  for (;;) {
    if (i <= 8) {
      sum += i;
    } else {
      break; // останавливает цикл
    }
    i++;
  } // или
  console.log(sum);
}

{
  let sum = 0;
  for (let j = 1; true ;) {
    if (j <= 8) {
      sum += j;
      j++;
    } else {
      break; // останавливает цикл
    }
  }
  console.log(sum);
}

// continue
{
  let sum = 0;
  for (let i = 1; i <= 8; i++) {
    if (i === 5) {
      continue; // пропускает итерацию
    }
    console.log('i:', i);
    sum += i;
  } // или
  console.log(sum);
}

// возведение в степень
Math.pow(2, 3) === 2 ** 3