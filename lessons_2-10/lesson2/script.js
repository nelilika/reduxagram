alert('Hello world');

// переменная которая начинается с is - логическая переменная
var isUserConfirmed = false;

// Правила трех НЕ
// 1) НЕ однобуквенные переменные
// 2) НЕ транслит - imia, dataRozdenia
// 3) НЕ зарезервированные слова

// 3 способа обозначения переменных
// camelCase - рекомендованный
// snake_Case
// kebab-case

// через var не пишем (!)

// let и const
let firstName = 'Liliia';
let lastName = 'Karty';

firstName = 'Sveta';

const birthDate = '11.10.1950'; // константа и изменена быть не может

// birthDate = '01.01.2034'; - приводит к ошибке

// prompt возвращает строку (тип string) ВСЕГДА (!)
const userFirstName = prompt('Enter your name'); // prompt('Enter your name', 'Liliia');
console.log(userFirstName); // вывод результата в консоль

// confirm возвращает логические true или false (тип boolean)
const isUserReadInfo = confirm('Did you read the information');
console.log(isUserReadInfo); 
console.log(typeof isUserReadInfo); // boolean

console.log(typeof null) // object

console.log(typeof firstName); // string



// немного математики

// 3 + 4 = 7; - math
const number1 = 3;
const number2 = 4;
const sum = number1 + number2; // операнд - бинарный оператор - операнд

let userNumber1 = prompt('Enter first number', '5');
console.log(userNumber1); // 5
console.log(typeof userNumber1); // string
let userNumber2 = prompt('Enter second number', '2');
console.log(userNumber2); // 2
console.log(typeof userNumber2); // string

userNumber1 = Number(userNumber1); // приведение строки в число через Number()
console.log(userNumber1); // 5
console.log(typeof userNumber1); // number

userNumber2 = +userNumber2; // приведение строки в число через УНАРНЫЙ оператор +
console.log(userNumber2); // 3
console.log(typeof userNumber2); // number

const result = userNumber1 % userNumber2;

console.log('Your result is: ', result);