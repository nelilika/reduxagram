/* 1) Напишіть програму, яка питає у користувача скільки йому років (команда prompt()) 
та виводить його вік на екран (команда alert()). */

const age = +prompt('How old are you?', 25);
alert(age);

/* 2) Напишіть програму, яка питає у користувача як його звати, в якій країні і в якому місті 
він живе (команда prompt()) та виведіть результат у консоль в наступному виді 
(команда console.log()). (Capture3.png)

`Hello, your name is ${name}. You live in ${city}, ${country}`
tips: для того, щоб вивести дані в консоль, як показано в прикладі, використовуємо 
зворотні кавички (``) і наступну конструкцію ${value}, де value є деякою змінною. */

const name = prompt('What is your name?', 'David');
const country = prompt('What country are you from?', 'Ukraine');
const city = prompt('What city are you from?', 'Kharkiv');

const result = `Hello, your name is ${name}. You live in ${city}, ${country}`;
console.log(result);

/*3) Напишіть програму, яка виводить в консоль (команда console.log()) 
тип даних 3х різних змінних.

Створіть змінну number та надайте їй значення 156798;
Створіть змінну string та надайте їй значення IT School Hillel;
Створіть змінну boolean та надайте їй значення false;
Відобразіть значення та її тип даних кожної змінної в консолі в наступному вигляді:

value: <value>; type: <type>
tips: використовуємо тіж самі зворотні кавички для виводу даних. */

const number = 156798;
const string = 'IT School Hillel';
const boolean = false;

console.log(`value: ${number}; type: ${typeof number}`);
console.log(`value: ${string}; type: ${typeof string}`);
console.log(`value: ${boolean}; type: ${typeof boolean}`);