/* Задача 1
Створіть три змінні.
Задайте першій з них будь-яке числове значення.
Друга змінна повинна дорівнювати першій, але збільшена в три рази.
Третя змінна повинна дорівнювати сумі перших двох.
Виведіть в консоль усі три змінні.  
*/

{
  const firstValue = 42;
  const secondValue = firstValue * 3;
  const thirdValue = firstValue + secondValue;

  console.log('First: ', firstValue);
  console.log('Second: ', secondValue);
  console.log('Third: ', thirdValue);
}

/* Задача 2
Cтворіть числові змінні х и y.
Значення їх задає користувач через команду prompt().
Підрахуйте добуток, ділення, різницю і суму цих змінних.
Результат відобразіть в консолі в форматі повної математичної операції:
a + b = c;
a - b = c;
a * b = c;
a / b = c;
*/

{
  const x = +prompt('Enter first value', '12');
  const y = +prompt('Enter first value', '12');

  let sum = x + y;
  let difference = x - y;
  let multiplication = x * y;
  let division = x / y;

  console.log(`x = ${x}, y = ${y}`);
  console.log(`${x} + ${y} = ${sum.toFixed(2)};`);
  console.log(`${x} - ${y} = ${difference.toFixed(2)};`);
  console.log(`${x} * ${y} = ${multiplication.toFixed(2)};`);
  console.log(`${x} / ${y} = ${division.toFixed(2)};`);
}

/* Задача 3
Створіть змінну str і задайте їх знамення з prompt.
Приведіть строку до верхнього і нижнього регістру, підрахуйте довжину 
і виведіть ці дані в консоль в наступному форматі
(все повинно бути записано в одному console.log()):

You wrote: "<str>" \ it's length <number of str characters>.
This is your big string: "<STR>".
	And this is a small one: "<str>"
*/

{
  let str = prompt('Enter you sting', 'hello world');

  console.log(`You wrote: "${str}" \\ it's length ${str.length}.\nThis is your big string: "${str.toUpperCase()}".\n\tAnd this is a small one: "${str.toLowerCase()}".`);
}

/* Задача 4
В одній пачці папіру 500 листів.
Офіс використовує 1200 листів папіру щотиджня.
Яку найменшу кількість папіру треба запути до офісу, щоб вистачило на 8 тижнів?
Результат відобразіть в консолі (команда console.log()).
*/

{
  const sheetsInReamPaper = 500;
  const consumptionPerWeek = 1200;
  const weeksAmount = 8;

  let totalSheets = consumptionPerWeek * weeksAmount;
  let packsForEightWeeks = totalSheets / sheetsInReamPaper;

  if (!Number.isInteger(packsForEightWeeks)) {
    packsForEightWeeks++;
  } // альтернатива Math.ceil(packsForEightWeeks)

  console.log(`You need ${packsForEightWeeks.toFixed()} packs for ${weeksAmount} weeks.`);
}

/* Задача 5 
Напишіть програму, яка рахує скільки порібно заплатити відсоткі від суми по кредиту.
Відсоток і суму задає користувач (команда prompt()),
результат виводимо в консоль (команда console.log()).
Враховуєте, що користувач може відправити 10 або 10.5%.
*/

{
  const percent = parseFloat(prompt('Enter percent of the credit:', '10%'));
  const sum = parseFloat(prompt('Enter sum of the credit:', '1000.5'));

  const percentFromSum = sum * percent / 100;

  console.log(`Percent of your credit is: ${percent}\nTotal sum of your credit is: ${sum}\nYour should pay: ${percentFromSum.toFixed(2)}.`);
}