/** Типы данных в JavaScript */
// 1. number
// 2. string
// 3. boolean
// 4. undefined
// 5. null
// 6. bigInt

// 7. symbol
// 8. object

// 1-7 - примитивы, 8 - объект

// Number - Числа
const integer = 345; // 123, 6830, 1, 45 ... целые числа
const double = 12.4; // 45.6, 56.745756, .6 === 0.6 ... дробные
const zero = 0; // +0, -0
const plusInfinity = Infinity; // любые связанные с ней мат. операции c числами приводят к Infinity;
const minusInfinity = -Infinity; // любые связанные с ней мат. операции c числами приводят к -Infinity;

// деление на 0 дает Infinity

const nan = NaN; // дословно not a number
// -Infinity + Infinity = NaN

const bigNumber = 1e9; // bigNumber === 1000000000; (x9)
const smallNumber = 1e-9; // smallNumber === 0.000000001; (x9)

// бинарная 1,0
const binarFive = 0b101; // (5) из десятичной системы в двоичной системе будет (101)
const hexFive = 0x19; // (25) из десятичной системы в шестнадцатиричной системе будет (19)

// Методы чисел

// number.toString()
// без параметров превратит число в строку
// с параеметрами превратит числов в строку в какой-то системе исчесления (2-30)
// toString(2) - двоичная
// toString(16) - шестнадцатиричная

// number.toFixed()
// обрезает количество знаков после запятой
// без параметров обрежет все знаки и вернет целое число в виде СТРОКИ
// с параметрами обставит заданное количество знаков и вернет дробное число в виде СТРОКИ
// toFixed() - целое
// toFixed(2) - два знака после запятой

// isNaN()
// возращает тип данных boolean, проверяет НЕ является ли заданное значение числом
// isNaN(3) - false, 3 - это число
// isNan('qwerty') - true, 'qwerty' нельзя представить в виде числа

// isFinite()
// возращает тип данных boolean, проверяет является ли заданное значение числом
// isNaN(3) - true, 3 - это число
// isNan('qwerty') - false, 'qwerty' нельзя представить в виде числа

// Неточные вычисления
// 0.1 + 0.2 НЕ РАВНО 0.3

// String
const singleQutesString = 'hello world'; // code style использовать одинарные кавычки
const doubleQuotesString = "hello world";
const backQuotesString = `hello world`;

// Экранирование - обратный слеш \
const ecranString = 'Hello \'Apple\'';

const username = 'fiolet';
const anotherExample = `Hello, \$\{\} ${username}!`; // 'Hello, ' + username + '!';

// '' и "" не работают с > 1 строкой
// `` работают

const enterString = 'hello\ndear\nStudents\n!'; // \n - энтер, переход на новую строку
const tabString = '\thello\ndear\nStudents\n!'; // \n - энтер, переход на новую строку

// Методы строк

// string.length - длина строки
// 'qwerty'.length - длина строки

// string.toUpperCase() - перевод строки в верхний регистр
// string.toLowerCase() - перевод строки в нижний регистр

// parseInt(string)
// возвращает ЦЕЛОЕ число из строки считывая ее слева направо
// parseInt('12$') // 12
// parseInt('$') // NaN

// parseFloat(string)
// возвращает число С ПЛАВАЮЩЕЙ ТОЧКОЙ из строки считывая ее слева направо
// parseFloat('12.4$') // 12.4
// parseFloat('$12') // NaN

// boolean
const trueBoolean = true;
const falseBoolean = false;

// undefined
// дословно обозначает - значение не задано
var someNewName1; // undefined
let someNewName2; // undefined

// null
// дословно обозначает - пустоту
someNewName1 = null; // null
someNewName2 = null; // null

// bigInt
// содержит большое число для которого нужна память больше чем 64бита
const bigIntNumber = 5n;

// Инкремент и Декримент
// увеличивает или уменьшает на 1 
let rawNumber = 345;

// постфиксная форма инкремента
// 345
rawNumber++; // rawNumber + 1 // 345
// rawNumber === 346
rawNumber++; // rawNumber + 1 // 346
// rawNumber == 347

// префиксная форма инкремента
// 345
++rawNumber; // rawNumber + 1 // 346
// rawNumber === 346
++rawNumber; // rawNumber + 1 // 347
// rawNumber == 347

// дeкремент
rawNumber--; // rawNumber - 1 - постфиксная форма
--rawNumber; // rawNumber -1 - префиксная форма

// let a = 1;
// a += 1 === ++a;

// = оператор присваивания
// == равно
// === строгое равно

// Сравнение строк - посимвольно слева направо

// от малого веса до большого
// символы %:"?."
// числа 0-9
// Большие буквы A-Z
// маленькие буквы a-z