/** 
 * Чем отличается let/const от var?
 * Что такое всплытие событий (Hosting)?
 * Что такое глобальная/локальная переменная?
 * Что такое (лексическая) область видимости?
 * Что такое замыкание?
*/


// проходится по ключевым словам function или var и 
// создает их
// function loop() {}; var userName4 = undefined; function filterArr(arr) {}

loop();

// Все способы объявления переменных
userName1 = 'username1';

var userName4 = 'username4';

let userName2 = 'username2';
const userName3 = 'username3';


// Отличие var от let
// 1) Область видимости var - фукнция (function() { })
//    Область видимости let - блок кода ({ })
// 2) Переменная var - "всплывает"
// то есть она может быть доступна до ее фактического объявления
// в ближайшей к ней функции
//    Переменная let - "не всплывает"

// Новая область видимости
{
  let name1 = 'name1'; // локальная переменная 
  console.log(name1); // глобальная
  console.log(userName1);
}
// console.log(name1); - выведет ошибку,
// переменная вне блока кода и ее тут не существует

{
  let name2 = 'name2';
}

// while (true) {

// }

for (let i = 0; i < 10; i++) {
  // console.log(i);
}

function loop() {
  // var j;
  console.log(j); // undefined
  for (var j = 0; j < 10; j++) { // 0
    let iterator = j;
    console.log(j);
  }
  console.log(j);
}
// console.log(j); - выведет ошибку,
// переменная вне функции и ее тут не существует

// console.log(i); - выведет ошибку,
// переменная вне блока кода и ее тут не существует

// if (true) {
//  
//}


// Область видимости - некий участок кода,
// в котором сущесвует переменная и к ней есть доступ

// Пример 1
// console.log(arr);
let arr = [1,2,3];
console.log(arr);
// arr = 2;
// arr.push(4); // [1,2,3,4];

//IFFE
function filterArr() {
  // let arr = [1,2,3];
  // let newArr = [];
  return arr.filter(item => !!item);
}
filterArr(arr);

// Пример 2
let number = 2;

function incrementNumber(number) {
  // объявление переменной let number = 2;
  number = 5;
  console.log('inside func: ', number);
}

incrementNumber(number);
console.log('outside func', number);

console.log(someValue); //undefined
if (true) {
  var someValue = 0;
}
console.log(someValue); //0