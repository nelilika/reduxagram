// Лексическая область видимости

// Global scope
/**
 * createStudent();
 * test();
 * student;
 */

const student = {
  firstName: 'Name',
  lastName: 'Surname',
  age: 25,
};
const name = 'name';

function test() {
  // Local scope test
  /**
   * ...Global scope
   * testValue;
   */
  const testValue = 345;
  console.log('TEST', student);
}
test();

// Local scope
function createStudent() {
  // Local scope createStudent
  /**
   * ...Global scope
   * student
   * getRandomInt();
   * logResult();
   * key
   * min
   * max
   */
  const student = {};
  let min = 15;
  let max = 25;

  student.name = 'April Value';
  student.age = getRandomInt(1, 10);

  min = 35;
  max = 45;
  getRandomInt(1, 10);

  min = 45;
  max = 55;
  getRandomInt(2, 20);

  min = 55;
  max = 65;
  getRandomInt(3, 30);

  function getRandomInt() {
    // let min = 1; - локальная переменная которая пришла из аргумента функции
    // let min = 15; - глобальная (по отношению к ф-ции getRandomInt) переменная, которая пришла с ф-ции createStudent
    // у ЛОКАЛЬНОЙ переменной высший приоритет!
    console.log(min); // 1
    // Local scope getRandomInt
    /**
     * min;
     * max;
     * ...Local scope createStudent(student, getRandomInt(), logResult(), key, min, max)
     * ...Global scope
     */
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function logResult(msg) {
    // Local scope logResult
    /**
     * ...Local scope createStudent
     * ...Global scope
     * msg
     */
    return `Log: ${msg}`;
  }

  for (let key in student) {
    console.log(logResult(student[key]));
  }

  console.log(student);
}
createStudent();

// Замыкания
function counterProtector() {
  let count = 0;
  let counterProtectorValue = 'counterProtectorValue';

  console.log(counterProtectorValue);

  function counterLocal() {
    // Local scope
    // count берет из функции counterProtector
    return ++count;
  }

  return counterLocal;
}

const counterGlobal = counterProtector();

console.log(counterGlobal());
console.log(counterGlobal());
console.log(counterGlobal());
console.log(counterGlobal());
console.log(counterGlobal());
console.log(counterGlobal());
console.log(counterGlobal());

// Мозговыносящее замыкание
{
  function counterProtector() {
    let count = 0;
    return function () {
      return ++count;
    };
  }
  const counterGlobal = counterProtector();

  console.log(counterGlobal());
  console.log(counterGlobal());
  console.log(counterGlobal());
}
