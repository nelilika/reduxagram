const value1 = 1;
let value2 = 2;
var value3 = 3;
value4 = 4; // создается глобальная переменная, bad practice (!)

// Приведение типов данных

// 1) number
// 2) string
// 3) boolean
// 4) null
// 5) undefined

// +, -, /, *, %,
// >, >=, <=, <
// =, ==, ===, !== (!=)

// остаток от деления (%)
// 5 : 2 = 2 (ост. 1) - математика
// 5 % 2 === 1 // true - JavaScript

// Логические операторы
// Логическое бинарное И (&&), Логическое унарное И (&)

// Логическое бинарное ИЛИ (||), Логическое унарное ИЛИ (|)
false || false || undefined || null // null

/** */
// Приведение типов данных в Boolean
// Number
// 0, NaN -> false
// 1, 34, -15, 1.4, 4.5, 1e5, Infinity -> true

Boolean(1) // true === !!1
Boolean(NaN) // false === !!NaN
Boolean(0) // false

// String
// '' -> false
// любая другая строка -> true
Boolean('') // false
Boolean(' ') // true

// Null / Undefined -> false
Boolean(null) // false 
Boolean(undefined) // false

// Приведение типов данных в Number
// Boolean
// false -> 0
// true -> 1

Number(false) // 0 == +false
Number(true) // 1

// String
// '12', '12 ' -> 12
// '1w2', 'w12', '12w',  -> NaN

Number('12') // 12
Number('12w') // NaN
Number('') // 0

// null -> 0
// undefined -> NaN
Number(null) // 0
Number(undefined) // NaN

// Приведение типов данных в String
// Boolean
// false -> 'false'
// true -> 'true'
String(false) // 'false'
String(true) // 'true'

// Number
// 0 -> '0'
// NaN -> 'NaN'
// 12 -> '12'

String(0) // '0'
String(12) // '12'

// null -> 'null'
// undefined -> 'undefined'
String(null) // 'null'
String(undefined) // 'undefined'


// Неявное преобразование данных
// мат операции делают неявное преобразование к числу

5 - 'qwerty' // Number(5), Number('qwerty') -> 5, NaN -> 5 - NaN -> NaN
5 - 'qwerty' // NaN

false * '12' // 0, 12 -> 0 * 12 -> 0
false * '12' // 0

undefined / '45.5' * 1 - 34 // NaN / 45.5 -> NaN

// НО
// +
1 + 3 // 4
false + true // 1
null + 45 // 45
3 + '3' // '33'
'34' + '45' // '3445'
'3' + 45 // '345'

// (3 + 5 + 0) + '56' // '856'
// 3 + 5 + (0 + '56') // '8056'

// false > 45 // false
// 'true' < true // false

'007' == 7 // true

Number('007') // 7

// сравнение с undefined всегда false
// сравнение с null всегда false

// КРОМЕ
// 1) undefined == undefined
// 1) null == null
// 2) null == undefined

// NaN == NaN -> всегда false

// null <= 0 // true
// null >= 0 // true

// Логическое НЕ (!)
// приводит к Boolean и возвращает противоположное значение
!'summer' // false
!null // true

// двойное неравенство
!!'summer' // true === Bolean('summer');
!!null // false === Boolean(null)

// задачка
alert() && 12 || 34 && null // null
