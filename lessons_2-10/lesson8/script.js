// Объект
const object = {
  name: 'Lesson 8',
  course: 'Front End pro',
  school: 'Hillel',
  isOnline: true,
  numberPresentStudents: 14,
};

for (let key in object) {
  // console.log('Object: ', object);
  // console.log(`Key: ${key}`);
  // console.log(`Value: ${object[key]}`);
}

// Массив
const array = [1, 2, 3, 4, 'y', 'i', 9, null, {}];

for (let value of array) {
  // console.log(value);
}

// Object.keys(object) -> ['name', 'course', 'school', 'isOnline', 'numberPresentStudents'] 
const objectKeys = Object.keys(object);
for (let key of objectKeys) {
  // console.log(key);
  // console.log(`Value: ${object[key]}`);
} // best practice по перебору объектов


// Функция

{
  let number = /*prompt('Enter a number', '1234567')*/ '1234567';
  let result = '';

  // const reverseNumber = function (numberAsStr, lastIndex) {
  //   let str = '';

  //   for (let i = numberAsStr.length - 1; i >= lastIndex; i--) {
  //     str += numberAsStr.charAt(i);
  //   }

  //   return str;
  // } // Function Expression

  if (number.charAt(0) !== '-') {
    result = reverseNumber(number, 0);
  } else {
    result = '-' + reverseNumber(number, 1);
  }

  if (result) {
    console.log(+result);
  }

  // Function Declaration
  function reverseNumber(numberAsStr, lastIndex) {
    console.log(arguments); // тоже самое что reverseNumber.arguments

    // for (let i = 0; i < arguments.length; i++) {
    //   console.log(arguments[i]);
    // }

    for (let value of arguments) {
      // console.log(value);
    }

    let str = '';

    if (!(typeof numberAsStr === 'string' && lastIndex >= 0)) {
      console.error('NOT VALID');
      return;
    }

    for (let i = numberAsStr.length - 1; i >= lastIndex; i--) {
      str += numberAsStr.charAt(i);
    }

    return str;
  }

  console.dir(reverseNumber); // возможность посмотреть функцию как объект
}

// как превратить псевдомассив в массив
// const arr = [...arguments];
// const arr = Array.from(arguments);
// console.log(arr);


// rest параметры
function sum(number1, ...args) {
  console.log(args);
  console.log(arguments);
  let sum = 0;
  // for (let i = 0; i <= arguments.length; i++) {
  //   sum += arguments[i];
  // }

  for (let number of args) {
    sum += number;
  }

  return sum;
}
console.log(sum(1, 2, 1));
console.log(sum(6, 3, 6));
console.log(sum(23456, 8347538));

// Стрелочная функция
const getSum = function(a, b) {
  return a + b;
} // анонимная функция, function expression, функция-выражение

const getSumArrow = (a, b) => {
  return a + b;
}; // стрелочная фунция

getSumArrow(1,2);
// в стрелочных функцих нет arguments

const getSumArrowWithoutReturn = (a, b) => (a + b); // стрелочная фунция


// Другие виды функций
// 1) Самовызывающая функция
// 2) Рекурсия
// 3) Функции планирования
// 4) Функции высшего порядка

// 1) самовызывающейся функция IIFE 
(function (name) {
  console.log('Hello, ', name); 
}('Admin')) // 

function getName(name) {
  return 'Hello, ' + name;
}

getName('Admin');

// 2) Рекурсивная функция
const callMe = function test() {
  console.dir(test);
  console.dir(callMe);
};
 
// function factorial(number) {
//   let result = 1;
//   for (let i = 2; i <= number; i ++) {
//     result *= i;
//   }
//   return result;
// }

function factorial(number) {
  let result = 1;
  for (let i = 2; i <= number; i ++) {
    result *= i;
  }
  return result;
}

function factorial(number) { // 5
  // 5 * factorial(4) -> 5 * 20
  // 4 * factorial(3) -> 
  // 3 * factorial(2) ->
  // 2 * factorial(1) -> 
  // 1 * factorial(0) -> 1 - 1 * 1

  return number > 0 ? number * factorial(number - 1) : 1;
}

console.log(factorial(5));

// 3) Функции высшего порядка
// функция высшего порядка - функции, которые работают с другими функциями
function highFunction(argument) {
  argument();

  return function() {
    console.log('Hello from return');
  };
}

const argumentFunction = function() {
  console.log('Hello from argument');
};

const returnFunction = highFunction(argumentFunction);

returnFunction();

