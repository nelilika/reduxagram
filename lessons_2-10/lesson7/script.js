/**
 * что такое Объект?
 * что такое ссылочный тип данных?
 * что такое глубокое копирование данных?
 * как работает преобразование типов данных для Объекта? 
 */

// Объект - {}
{
  let value = 4;
  let string = '4564';

  value = 7;
  string = 'Hello';

  let obj = {
    key: 'value',
    value: 7,
    string: 'Hello',
    a: {
      qwerty: 'qwerty',
    }
  };

  obj.value === 7;
  console.log(obj.key); // 'value'

  obj.test = 'Im a test key';

  console.log(obj);

  obj = { };

  console.log(obj);
}

// значимый тип данных
{
  let number1 = 23;
  let number2 = number1; // данные передаются по значению

  console.log(number1);
  console.log(number2);

  number1 = 12;
  console.log('');

  console.log(number1);
  console.log(number2);

  // ссылочный тип данных
  let obj1 = {
    name: 'Anna',
    age: 25,
  };

  // obj1.test; // undefined

  let obj2 = obj1; // данные передаются по ссылке

  console.log(obj1); // obj1 и obj2 ссылаются на одно и то же место в память
  console.log(obj2);

  obj1['age'] = 34; // тоже самое, что и obj1.age
  console.log('');

  console.log(obj1);
  console.log(obj2);
}

// Примеры
{
  const obj1 = {};
  const obj2 = {};
  const obj3 = obj1;

  obj1 === obj2 // false
  obj1 === obj3 // true
  obj2 === obj3 // false
}

// Массив - это упорядоченная коллекция данных - []
{
  const arr = [1, '2', false, {}, [], null, undefined, NaN, Infinity, 0, '', 2n];
  console.log(arr);

  console.log(arr[4]); // []
  console.log(arr[7]); // NaN
  console.log(arr[67]); // undefined

  arr[0] = 2;
  console.log(arr.length);
  arr[arr.length - 1] = '2n'; // последний элемент массива  - index11

  arr[13] = 1; // arr[12] - empty
}
// Удаление объектов
{
  const obj = {a: 1};
  delete a;
  const arr = [1,2,3];
  delete arr[2]; // НИКОГДА ТАК НЕ ДЕЛАЕМ!!!
}

// Копирование объектов
// spread оператор ...
// Object.assign() 
{
  let obj1 = {
    name: 'Anna',
    age: 25,
  };
  
  let obj2 = { surname: 'Kowalckih ', ...obj1 }; // копирование объекта без копирование его ссылки
  let obj3 = Object.assign({ surname: 'Kowalckih '}, obj1); // копирование объекта без копирование его ссылки
  
  console.log(obj1); // obj1 и obj2 ссылаются на одно и то же место в память
  console.log(obj2);
  console.log(obj3);
  
  obj1['age'] = 34; // тоже самое, что и obj1.age
  console.log('');
  
  console.log(obj1);
  console.log(obj2);
  console.log(obj3);  
}

{
  const manDog = {
    breed: 'Biegl',
    name: 'Tor',
    age: 2,
    isHomeless: false,
    sex: 'male',
    location: {
      country: 'Ukraine',
      city: 'Kharkiv',
      q: {}
    },
  };
  
  const womenDog  = {
    ...manDog,
    sex: 'female',
    age: 2,
    name: 'Torina',
    location: {
      ...manDog.location,
      q: {
        ...manDog.location.q,
      }
    },
  };
  
  womenDog.breed = 'Biagle';
  womenDog.location.city = 'Kyiv';
  console.log(womenDog);
  // spread оператор делает только ПОВЕРХНОСТНУЮ копию
}

// Глубокая копия объекта
// JSON.parse() - строку в объект
// JSON.stringify() - объект в строку

{
  const manDog = {
    breed: 'Biegl',
    name: 'Tor',
    age: 2,
    isHomeless: false,
    sex: 'male',
    location: {
      country: 'Ukraine',
      city: 'Kharkiv',
      q: {}
    },
  };

  const womenDogSTR = JSON.stringify(manDog); // превратили объект manDog в строку
  const womenDog = JSON.parse(womenDogSTR);// превратили строку в объект

  
  womenDog.breed = 'Biagle';
  womenDog.name = 'Torina';
  womenDog.location.city = 'Kyiv';
  console.log(womenDog);
  console.log(manDog);
  // JSON помогает сделать Глубокое копирование объектов
}

// Приведение Объектов в разные типы данных
/** */
// Приведение в Boolean
// всегда true

Boolean({}); // true
Boolean({ a: 1 }); // true

// Приведение в Number
// нет такого преобразования, потому что
// obj.valueOf() -> obj - не возвращает число при попытке представить объект
// в качестве численного примитива

// Приведение в String
// obj.toString() -> '[object Object]'
// '[object Object]' - это строковое примитивное представление объекта

// Примеры
Boolean({ a: 1 }) // true
Number({}) // Number('[object Object]') -> NaN 
'$' + { hello: 'world' } // $[object Object]
+{ key: 'value' } // NaN


// Приведение Mассива в разные типы данных
/** */
// Приведение в Boolean
// всегда true
Boolean([]); // true
Boolean([1,2,3]); // true

// Приведение в Number
// нет такого преобразования, потому что
// arr.valueOf() -> arr - не возвращает число при попытке представить массив
// в качестве численного примитива

// Приведение в String
// arr.toString() -> ''
// '' - это строковое примитивное предстваление пустого массива

String([]); // ''
String([1,2,3]); // '1,2,3'
String([null, 1, 'qwerty']); // ',1,qwerty'
String([undefined, false]); // ',false'

// Пример
// [1,undefined,'qwe'] + 34 + '' // '1,,qwe34'
// !!Boolean([1]) // true
// [] + {} // '[object Object]'
// {} + [] // 0

// for .. in - для объектов только!

// а что для массивов? 

const string = 'hello, world';
const arr = [1, 2, 3, 4, 5, '6', null, NaN];
for (let i = 0; i < 10; i++) {
  console.log(arr[i]);
  console.log(string[i]);
}



