(null >= 0) + 5 // 6
("12" < "true") + parseFloat('13.5$') // 14.5
null - 5 + ("null - 5") + "false" // '-5null - 5false'
true - true + "hello" === "0hello" // true
null + " is " + (typeof null)  // 'null is object'
null + " is " + (typeof null) // 'null is [object Object]'
Boolean(null) + !Boolean("") / Boolean(1) + Boolean(undefined) // 1
"null" + +("hello" > "Hello") - Number("3") // NaN
34 + "43" - parseInt("43px") // 3400
String(false) - String(true) + " not " + NaN // NaN not Nan
("text" > 'Text') + 5 + null * 45 // 6
!!parseFloat("t123") - false + true // 1
"hello" && 0 - '22' // -22
"true" + (Boolean('false') / 0) // 'trueInfitinity'
(undefined == null) + +'12' + '13' / 1 // 26