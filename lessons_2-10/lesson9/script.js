const arr1 = [];
const arr2 = new Array(2); // [empty, empty]

// Очищение массива
let arr3 = [1,2,3,4];
arr3 = [];
arr3.length = 0; // ОЧЕНЬ плохой код, не надо так

const arrayTest = ['a', 'b', 'c'];

function changeArray(item, index, test) {
 // put some logic here
}

changeArray('a', 0, ['a', 'b', 'c']);
changeArray('b', 1, ['a', 'b', 'c']);
changeArray('c', 3, ['a', 'b', 'c']);

// item - элемент массива
// index - индекс массива
// array - сам массив, который проганяется в цикле


// Array For Each (замена цикла for/while/do while/for of)
// перебор масисва по циклу

arrayTest.forEach(function(arrItem, i, arr) {
  // console.log(arguments);
});

/* */
// student {
//  firstName,
//  lastName,
//  dateOfBirth,
//  course:
//  groupNumber,
//  averageMark,
//  isDormitory,
// }

let students = new Array(15);

// Array Fill(value)
// заполняет массив одним значением value

students.fill(null);
// delete students[5];

// [null, null, null, null, ..]
// [1,2,3,4,5...]

// Array Map(function(item, index, array) { return (!) /* some logic */ })
{
  const newArrayAfterMap = students.map(function(item, index, arr) {
    return item; // новое значение элемента массива
  });
}
// const newArrayAfterMap = [..students];

const names = `
Scarlett Mercado
Grayson Rowley
Iris Vega
Hester Corona
Regan Coulson
Hunter Hernandez
Maison Currie
Lydia Wormald
Rebecca Byrne
Zoha Christie
Ava-Grace Edmonds
Baran Lutz
Kaycee Dunlap
Patricia Salinas
Zacharia Ball
`;
const courses = ['Front End Basic', 'Front End Pro', 'Pyton', 'UX/UI'];

const newArrayAfterMap = students.map(function(item, index, arr) {
  return {
    firstName: 'firstName',
    lastName: 'lastName',
    dateOfBirth: new Date(),
    course: 'course',
    groupNumber: 0,
    averageMark: 0,
    isDormitory: false,
  };
});

// String Split(some params)
// разбивает строку по указаному параметру

// String Trim()
// удаляет пробельные символы из начала и конца строки

const namesWithoutSpaces = names.trim();
const namesArrayTest = namesWithoutSpaces.split('\n');

const namesArray = names.trim().split('\n');

const namesArrayAfterMap = namesArray.map(function(item, index, arr) {
  // 'Scarlett Mercado'
  // 'Grayson Rowley'
  const [firstName, lastName] = item.split(' '); // деструктуризация

  return {
    firstName,
    lastName,
    dateOfBirth: new Date(
      getRandomIntInclusive(1960, 2000),
      getRandomIntInclusive(0, 11),
      getRandomIntInclusive(0, 28),
    ),
    course: courses[getRandomIntInclusive(0, courses.length - 1)],
    groupNumber: getRandomIntInclusive(1, 10),
    averageMark: getRandomIntInclusive(0, 100),
    isDormitory: setIsDormitory(),
  };
});

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setIsDormitory() {
  return Math.random() > 0.5;
}

