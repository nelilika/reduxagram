import { combineReducers } from 'redux';
import { commentsReducer } from './comments';
import { postReducer } from './posts';

export const rootReducer = combineReducers({
  comments: commentsReducer,
  posts: postReducer,
});
