import { LOAD_POSTS, SELECT_POST, LIKE_POST } from '../actions/posts';

export const initialState = {
  posts: [],
  selectedPost: {},
};

export const postReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOAD_POSTS:
      return {
        ...state,
        posts: [...action.payload.posts],
        selectedPost: action.payload.posts[0],
      };
    case SELECT_POST:
      return {
        ...state,
        selectedPost: { ...action.payload.post },
      };
    default:
      return state;
  }
};
