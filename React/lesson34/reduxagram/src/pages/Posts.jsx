import React, { useEffect } from 'react';
import PostGrid from '../components/Posts/PostGrid';
import Typography from '@mui/material/Typography';
import { getPosts } from '../api';
import { useDispatch, useSelector } from 'react-redux';
import { loadPosts } from '../actions/posts';

export default function Posts() {
  const { posts } = useSelector((state) => state.posts);
  const dispatch = useDispatch();

  useEffect(() => {
    getPosts({ limit: 12, page: 1 }).then((loadedPosts) =>
      dispatch(loadPosts(loadedPosts.data))
    );
  }, [dispatch]);

  return (
    <>
      <Typography
        variant="h1"
        component="h2"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Reduxagram
      </Typography>
      <PostGrid posts={posts} />
    </>
  );
}
