export const LOAD_POSTS = '[POST] Load Posts';
export const LIKE_POST = '[POST] Like Post';
export const SELECT_POST = '[POST] Select Post';

export const loadPosts = (posts) => ({
  type: LOAD_POSTS,
  payload: { posts },
});

export const likePost = (postId) => ({
  type: LIKE_POST,
  payload: { postId },
});

export const selectPost = (post) => ({
  type: SELECT_POST,
  payload: { post },
});
