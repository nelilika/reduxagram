import { createLogger } from 'redux-logger';

const logger = createLogger();

const customLogger = (store) => (next) => (action) => {
  console.warn('prev state:', store.getState());
  console.log('action:', action);
  next(action);
  console.error('next state', store.getState());
};

const middlewares = [];

if (process.env.NODE_ENV === 'development') {
  middlewares.push(logger);
}

export { middlewares };
