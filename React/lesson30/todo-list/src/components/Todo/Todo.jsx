import { useState, useContext } from "react";
import { FiEdit2, FiX } from "react-icons/fi";
import AddTodo from "../AddTodo/AddTodo";
import { TodoContextComponent } from "../../context/Context";

function Todo({ todo }) {
  const [editedTodo, setEditedTodo] = useState(null);
  const { completeTodo, removeTodo, updateTodo } =
    useContext(TodoContextComponent);

  let classes = ["todo-row"];

  if (todo.isCompleted) {
    classes = [...classes, "complete"];
  }

  if (editedTodo) {
    return (
      <AddTodo
        editedTodo={editedTodo}
        setEditedTodo={setEditedTodo}
        updateTodo={updateTodo}
      />
    );
  }

  return (
    // <div className={todo.isCompleted ? 'todo-row complete' : 'todo-row'}>
    <div className={classes.join(" ")}>
      <div onClick={() => completeTodo(todo.id)}>{todo.text}</div>
      <div className="icons">
        <FiX onClick={() => removeTodo(todo.id)} />
        {!todo.isCompleted && (
          <FiEdit2 onClick={() => setEditedTodo({ ...todo })} />
        )}
      </div>
    </div>
  );
}

export default Todo;
