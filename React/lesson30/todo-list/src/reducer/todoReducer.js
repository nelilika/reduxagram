export const initialState = {
  todos: [],
};

export const ADD_TODO = "[TODO] Add Todo";
export const REMOVE_TODO = "[TODO] Remove Todo";
export const COMPLETE_TODO = "[TODO] Complete Todo";
export const UPDATE_TODO = "[TODO] Update Todo";

/*
 action = {
    type: '[TODO] Add Todo' || '[TODO] Remove Todo' || '[TODO] Complete Todo'
    payload: todo
 }
*/

export const todoReducer = (state = initialState, action) => {
    console.log(action);
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [action.payload.todo, ...state.todos],
      };
    case REMOVE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload.todoId),
      };
    case COMPLETE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) => {
          if (todo.id === action.payload.todoId) {
            return {
              ...todo,
              isCompleted: !todo.isCompleted,
            };
          }
          return todo;
        }),
      };
    case UPDATE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload.todo.id ? action.payload.todo : todo
        ),
      };
    default:
      return state;
  }
};
