import { useContext } from "react";
import "./App.scss";
import TodoList from "./components/TodoList/TodoList";
import AddTodo from "./components/AddTodo/AddTodo";
import { TodoContextComponent } from "./context/Context";

function App() {
  const [{ todos }] = useContext(TodoContextComponent);

  // кеширование данних
  // const sortedTodos = useMemo(() => {
  //   return [...todos].sort((todo) => (todo.isCompleted ? 1 : -1));
  // }, [todos]);

  // const sortedTodos = [...todos].sort((todo) => todo.isCompleted ? 1 : -1 );

  // function completeTodo(todoId) {
  //   dispatch({
  //     type: COMPLETE_TODO,
  //     payload: { todoId },
  //   });
  // }

  // function removeTodo(todoId) {
  //   dispatch({
  //     type: REMOVE_TODO,
  //     payload: { todoId },
  //   });
  // }

  // function updateTodo(todo) {
  //   dispatch({
  //     type: UPDATE_TODO,
  //     payload: { todo },
  //   });
  // }

  return (
    <div className="todo-app">
      <h1>What's plan for Today, bro?</h1>
      <AddTodo />
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
