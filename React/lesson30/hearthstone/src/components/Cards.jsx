import Card from "./card/Card";

function Cards({ cards }) {
  return cards.map((card) => (
    <Card key={card.dbfId} card={card} />
  ));
}

export default Cards;
