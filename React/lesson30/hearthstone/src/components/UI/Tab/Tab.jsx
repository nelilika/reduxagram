import './Tab.scss';

function Tab({ tabs, activeTabIndex }) {
    return (
      <ul className="tabs group">
        {
            tabs.map((tab, i) => (
             <li key={i}>
                <span className={activeTabIndex === i && 'active'}>{tab}</span>
              </li>
            ))
        }
      </ul>
    );
}

export default Tab;