import './Card.scss';

function Card({ card }) {
  return (
    <div className="card-item">
      <h3> { card.name }</h3>
      <p> { card.text }</p>
    </div>
  );
}

export default Card;
