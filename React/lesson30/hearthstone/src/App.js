import "./App.scss";
import { useEffect, useState } from 'react';
import Cards from './components/Cards';
import Tab from './components/UI/Tab/Tab';
import { getHSInfo, getHSCardsByQuality } from './api/hearthstoneAPI';

function App() {
  const [cards, setCards] = useState([]);
  const [qualities, setQualities] = useState([]);
  const [activeTabIndex, setActiveTabIndex] = useState(0);

  useEffect(() => {
    getHSInfo().then(qualities => setQualities(qualities));
    getHSCardsByQuality('Legendary').then(cards => setCards(cards));
  }, []);

  return (
    <>
      <h1>Hearthstone</h1>
      <div className="wrap">
        <Tab tabs={qualities} activeTabIndex={activeTabIndex}/>
        <div id="content">
          {cards.length && <Cards cards={cards}/> }
        </div>
      </div>
    </>
  );
}

export default App;
