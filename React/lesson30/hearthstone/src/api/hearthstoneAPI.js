import axios from "axios";
import { API_HOST, API_KEY } from "../config";

const baseHSconfig = axios.create({
  baseURL: `https://${API_HOST}`,
  headers: {
    "X-RapidAPI-Key": API_KEY,
    "X-RapidAPI-Host": API_HOST,
  },
});

export async function getHSInfo() {
  const {
    data: { qualities },
  } = await baseHSconfig.get("/info");
  return qualities;
}

export async function getHSCardsByQuality(quality, locale = "ruRU", cost = 10) {
  const { data } = await baseHSconfig.get(`/cards/qualities/${quality}`, {
    params: {
      locale,
      cost,
    },
  });
  return data;
}
