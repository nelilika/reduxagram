import axios from 'axios';
import { ACCESS_KEY } from '../config/config';

const unsplashAPI = axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: `Client-ID ${ACCESS_KEY}`,
    },
});

export const searchPhotos = (query, per_page = 20) => {
    return unsplashAPI.get('/search/photos', {
        params: { query, per_page },
    });
}