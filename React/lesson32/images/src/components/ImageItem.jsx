import { Component, createRef } from "react";

const ROW_HEIGHT = 10;

export default class ImageItem extends Component {
  constructor() {
    super();

    this.imageRef = createRef();

    this.state = {
        span: 0,
        isImageLoaded: false,
    }
  }

  componentDidMount() {
    const imageDOMElement = this.imageRef.current;
    console.log(this.imageRef.current.clientHeight); // тут буде 0, бо зображення ще не загружене

    imageDOMElement.addEventListener('load', () => {
        const height = this.imageRef.current.clientHeight;
        this.setState({ span: Math.ceil(height / ROW_HEIGHT), isImageLoaded: true })
    })
  }

  render() {
    const {
      alt_description: alt,
      urls: { regular: src },
    } = this.props.image;

    return <img
        style={{ gridRowEnd: `span ${this.state.span}`}}
        ref={this.imageRef}
        src={src}
        alt={alt}
    />;
  }
}
