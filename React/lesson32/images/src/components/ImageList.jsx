import { Component } from "react";
import ImageItem from "./ImageItem";

class ImageList extends Component {
  constructor() {
    super();
  }

  render() {
    if (!this.props.images.length) {
      return <h4> No results </h4>;
    }

    return (
      <div className="image-list">
        {this.props.images.map((image) => (
          <ImageItem key={image.id} image={image} />
        ))}
      </div>
    );
  }
}

export default ImageList;
