import { Component, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { solid } from "@fortawesome/fontawesome-svg-core/import.macro";

// function SearchPhoto() {
//     const [searchInput, setSearchInput] = useState('');
//     function onChangeSearch(e) {
//         setSearchInput(e.target.value);
//     }
// }

class SearchPhotos extends Component {
  constructor() {
    super();
    this.state = {
      searchInput: "",
    };

    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  // втрата контексту
    onChangeHandler(event) {
      // console.log(event.target.value);
      // console.log(this); <--- this === undefined
      this.setState({
          searchInput: event.target.value,
      });
    }

  // arrow function
  //   onChangeHandler = (event) => {
  //     this.setState({
  //       searchInput: event.target.value,
  //     });
  //   }

  onFormSubmit = (event) => {
    event.preventDefault();
    console.log(this.props);
    this.props.onSearchSubmit(this.state.searchInput);
    this.setState({
      searchInput: '',
  });
  }

  render() {
    return (
      <div className="wrap">
        <form className="search" onSubmit={this.onFormSubmit}>
          <input
            type="text"
            value={this.state.searchInput}
            onChange={this.onChangeHandler}
            className="searchTerm"
            placeholder="What images are you looking for?"
          />
          <button className="searchButton">
            <FontAwesomeIcon icon={solid("search")} />
          </button>
        </form>
      </div>
    );
  }
}

export default SearchPhotos;
