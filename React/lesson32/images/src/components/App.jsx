import { Component } from 'react';
import SearchPhotos from './SearchPhoto';
import ImageList from './ImageList';
import { searchPhotos } from '../api/unsplash';
import '../styles/App.css';

/* Функціональний компонент */
// function App() {
//     return <button>Click me</button>
// }

/* Класовий компонент */
class App extends Component {
    constructor() {
        super();
        this.state = {
            images: [],
        }
    }

    onSearchSubmit = async (query) => {
        const { data: { results: images } } = await searchPhotos(query);
        console.log(images);
        this.setState({ images });
    }

    render() {
        return (
            <>
                <SearchPhotos onSearchSubmit={this.onSearchSubmit} />
                <ImageList images={this.state.images} />
            </>
        );
    }
}

export default App;