import { getPosts, getPost } from "../api";
import { loadPostsSuccess, loadPostsError, loadPostSuccess, loadPostError } from "../actions/posts";

export const fetchPosts = (params) => {
  return async (dispatch) => {
    try {
      const { data, headers } = await getPosts(params);
      const totalCount = +headers['x-total-count'];
      dispatch(loadPostsSuccess(data, totalCount));
    } catch (err) {
      dispatch(loadPostsError(err));
    }
  };
};

export const fetchPostById = (id) => {
    return async (dispatch) => {
      try {
        const { data } = await getPost(id);
        dispatch(loadPostSuccess(data));
      } catch (err) {
        dispatch(loadPostError(err));
      }
    };
  };
