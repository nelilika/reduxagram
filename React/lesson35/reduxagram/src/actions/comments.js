export const commentsActions = {
  LOAD_COMMENTS: '[Comments] Load Comments',
  ADD_COMMENT: '[Comments] Add Comment',
};

export const loadComments = (comments) => ({
  type: commentsActions.LOAD_COMMENTS,
  payload: comments,
});

export const addComment = (comment) => ({
  type: commentsActions.ADD_COMMENT,
  payload: comment,
});
