export const LOAD_POSTS_SUCCESS = '[POST] Load Posts Success';
export const LOAD_POSTS_ERROR = '[POST] Load Posts Error';
export const LIKE_POST = '[POST] Like Post';
export const LOAD_POST_SUCCESS = '[POST] Load Post Success';
export const LOAD_POST_ERROR = '[POST] Load Post Error';

export const loadPostsSuccess = (posts, totalCount) => ({
  type: LOAD_POSTS_SUCCESS,
  payload: { posts, totalCount },
});

export const loadPostsError = (error) => ({
  type: LOAD_POSTS_ERROR,
  payload: { error },
});

export const likePost = (postId) => ({
  type: LIKE_POST,
  payload: { postId },
});

export const loadPostSuccess = (post) => ({
  type: LOAD_POST_SUCCESS,
  payload: { post },
});

export const loadPostError = (post) => ({
  type: LOAD_POST_ERROR,
  payload: { post },
});
