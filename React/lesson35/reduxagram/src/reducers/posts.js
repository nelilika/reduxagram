import { LOAD_POSTS_SUCCESS, LOAD_POST_SUCCESS, LIKE_POST } from '../actions/posts';

export const initialState = {
  posts: [],
  selectedPost: {},
  limit: 5,
  page: 1,
  totalCount: 0,
};

export const postReducer = function (state = initialState, action) {
  switch (action.type) {
    case LOAD_POSTS_SUCCESS:
      return {
        ...state,
        posts: [...action.payload.posts],
        selectedPost: action.payload.posts[0],
        totalCount: action.payload.totalCount,
      };
    case LOAD_POST_SUCCESS:
      return {
        ...state,
        selectedPost: { ...action.payload.post },
      };
    default:
      return state;
  }
};
