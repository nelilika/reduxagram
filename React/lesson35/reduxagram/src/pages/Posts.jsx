import React, { useEffect, useState } from 'react';
import PostGrid from '../components/Posts/PostGrid';
import Typography from '@mui/material/Typography';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPosts } from '../thunk/posts';
import PostPagination from '../components/UI/Pagination';
import { getPages } from '../utils/getPages';
import { useMemo } from 'react';

export default function Posts() {
  const { posts, limit, totalCount } = useSelector((state) => state.posts);
  const [page, setPage] = useState(1);
  const dispatch = useDispatch();

  const pages = useMemo(() => getPages(limit, totalCount), [totalCount, limit]);

  useEffect(() => {
    dispatch(fetchPosts({ limit, page }));
  }, [dispatch, page]);

  function changePage(_, value) {
    setPage(value);
  }

  return (
    <>
      <Typography
        variant="h1"
        component="h2"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Reduxagram
      </Typography>
      <PostPagination sx={{ display: 'flex', alignItems: 'center' }} count={pages} page={page} changePage={changePage}/>
      <PostGrid posts={posts} />
    </>
  );
}
