import React from 'react';
import Typography from '@mui/material/Typography';
import LoginForm from '../../components/Login/LoginForm';
import './Login.scss';

function Login() {
  return (
    <div className="login-wrapper">
      <Typography
        variant="h2"
        component="h3"
        sx={{
          textAlign: 'center',
          fontFamily: 'Festive',
          color: 'lightBlue',
          m: '15px 0',
        }}
      >
        Welcome to Reduxgram
      </Typography>

      <LoginForm />
    </div>
  );
}

export default Login;
