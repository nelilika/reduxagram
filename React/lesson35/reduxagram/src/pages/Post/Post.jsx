import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { red } from "@mui/material/colors";
import Comments from "../../components/Comments/Comments";
import AddNewComment from "../../components/Comments/AddNewComment";
import Button from "@mui/material/Button";
import { useParams } from "react-router-dom";
import "./Post.scss";
import { getComments } from "../../api";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostById } from "../../thunk/posts";

function Post() {
  const { id } = useParams();
  const navigate = useNavigate();
  const { selectedPost: post } = useSelector((state) => state.posts);
  const dispatch = useDispatch();

  const [comments, setComments] = useState([]);

  useEffect(() => {
    if (Object.keys(post)) {
      dispatch(fetchPostById(id));
    }
    getComments(id).then((comments) => setComments(comments.data));
  }, [id]);

  function goBack() {
    navigate(-1);
  }

  return (
    <div className="post-wrapper">
      <div className="post">
        <Card
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
          }}
        >
          <CardHeader
            avatar={
              <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                <img
                  style={{ width: "100%" }}
                  src="/cat-logo.webp"
                  alt="avatar"
                ></img>
              </Avatar>
            }
            title={post.username}
            subheader={post.createdOn}
          />
          <div className="post-info">
            <CardMedia
              component="img"
              image={post.display_src}
              alt="Paella dish"
              sx={{ width: "500px" }}
            />
            <div className="post-comments">
              <div className="comments">
                <Comments comments={comments} />
              </div>
              <div>
                <AddNewComment selectedPost={post} />
              </div>
            </div>
          </div>
          <CardContent>
            <Typography variant="body2" color="text.secondary">
              {post.caption}
            </Typography>
            <Button sx={{ margin: '50px 0 0 0' }} variant="outlined" onClick={goBack}>
              Go back
            </Button>
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default Post;
