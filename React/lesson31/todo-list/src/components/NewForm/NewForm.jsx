import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import Button from "@mui/material/Button";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormLabel from "@mui/material/FormLabel";
import TodoInput from "../UI/Input/Input";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { createRef, useState } from 'react';

const schema = yup
  .object({
    username: yup
      .string()
      .matches(/^[a-zA-z!]+$/i, "Username should be only aplhabetical or !")
      .required("Username is required"),
    adress: yup.string().required(),
    phone: yup.number().positive().integer().required(),
  })
  .required();

function NewForm(props) {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  const [img, setImg] = useState({
    src: '',
    name: '',
  })

  const onSubmit = (data) => console.log(data);

  const updateImage = (event) => {
    console.log(event);

    const reader = new FileReader();
    const file = event.target.files[0];
    console.log(file);

    if (file) {
      reader.readAsDataURL(file);
    }

    reader.onloadend = () => {
      console.log(reader.result);
      setImg({
        src: reader.result,
        name: file.name,
      })
    }
  }

  const fileInputRef = createRef();

  function choseFile() {
    fileInputRef.current.click();
  }

  // console.log(watch("username")); // watch input value by passing the name of it

  return (
    <Box
      sx={{
        width: 300,
        background: "white",
        p: "10px",
        margin: "0 auto",
      }}
    >
      <form sx={{ width: "100%" }} onSubmit={handleSubmit(onSubmit)}>
        <TodoInput {...register("username")} placeholder="Username" />
        <p>{errors.username?.message}</p>
        <TodoInput {...register("adress")} placeholder="Adress" />
        <TodoInput {...register("phone")} placeholder="Phone" />

        <FormLabel id="demo-radio-buttons-group-label">Gender</FormLabel>
        <RadioGroup
          aria-labelledby="demo-radio-buttons-group-label"
          defaultValue="female"
          name="radio-buttons-group"
          sx={{ display: "flex", flexDirection: "row" }}
        >
          <FormControlLabel value="female" control={<Radio />} label="Female" />
          <FormControlLabel value="male" control={<Radio />} label="Male" />
          <FormControlLabel value="other" control={<Radio />} label="Other" />
        </RadioGroup>
        <Button fullWidth sx={{ marginBottom: "10px" }} onClick={choseFile}>
          Choose File
        </Button>
        <label>
          <TextField
            sx={{ display: "none" }}
            type="file"
            {...register("avatar")}
            onChange={updateImage}
            ref={fileInputRef}
          />
        </label>
        <img style={{ maxWidth: 400 }} src={img.src} alt={img.name} />
        <p>{img.name}</p>
        <button type="submit" variant="contained">
          Create something
        </button>
      </form>
    </Box>
  );
}

export default NewForm;
