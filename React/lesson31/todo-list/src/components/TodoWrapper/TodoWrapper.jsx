import { useContext } from "react";
import "./TodoWrapper.scss";
import TodoList from "../TodoList/TodoList";
import AddTodo from "../AddTodo/AddTodo";
import { TodoContextComponent } from "../../context/Context";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { toggleModal } from "../../reducer/todoReducer";
import SimpleDialog from '../UI/Dialog/Dialog';
import Button from '@mui/material/Button';
import NewForm from "../NewForm/NewForm";

function TodoWrapper() {
  const [{ todos, isOpenModal }, dispatch] = useContext(TodoContextComponent);
  const user = true;

  const handleToggleModal = () => {
    dispatch(toggleModal());
  }

  return (
    <div className="todo-app">
      {user ? (
        <div className="user-header">
          <Avatar alt="Remy Sharp" src="happy-cat.jpg" />
          <Typography className="header" variant="h5" component="h5">
            Welcome back, *username*
          </Typography>
        </div>
      ) : (
        <h1 className="header" variant="h5" component="h5">
          What's plan for Today, bro?
        </h1>
      )}
      <AddTodo />
      <Button onClick={handleToggleModal} variant="text">Add user</Button>
      <TodoList todos={todos} />
      <SimpleDialog open={isOpenModal} onClose={handleToggleModal}>
          <NewForm />
      </SimpleDialog>
    </div>
  );
}

export default TodoWrapper;
