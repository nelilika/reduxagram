export const initialState = {
  todos: [],
  isOpenModal: true,
};

const ADD_TODO = "[TODO] Add Todo";
const REMOVE_TODO = "[TODO] Remove Todo";
const COMPLETE_TODO = "[TODO] Complete Todo";
export const UPDATE_TODO = "[TODO] Update Todo";

const TOGGLE_MODAL = "[MODAL] Toggle Modal";

/*
 action = {
    type: '[TODO] Add Todo' || '[TODO] Remove Todo' || '[TODO] Complete Todo'
    payload: todo
 }
*/

// action function
export const addTodo = (todo) => ({
  type: ADD_TODO,
  payload: { todo },
});

export const completeTodo = (todoId) => ({
  type: COMPLETE_TODO,
  payload: { todoId },
});

export const removeTodo = (todoId) => ({
  type: REMOVE_TODO,
  payload: { todoId },
});

export const toggleModal = () => ({
  type: TOGGLE_MODAL,
});

export const todoReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [action.payload.todo, ...state.todos],
      };
    case REMOVE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload.todoId),
      };
    case COMPLETE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) => {
          if (todo.id === action.payload.todoId) {
            return {
              ...todo,
              isCompleted: !todo.isCompleted,
            };
          }
          return todo;
        }),
      };
    case UPDATE_TODO:
      return {
        ...state,
        todos: state.todos.map((todo) =>
          todo.id === action.payload.todo.id ? action.payload.todo : todo
        ),
      };
    case TOGGLE_MODAL:
      return {
        ...state,
        isOpenModal: !state.isOpenModal,
      }
    default:
      return state;
  }
};
