function Card({ card }) {
  return (
    <h1 style={{ color: 'white'}}> {card.original_name }</h1>
  );
}

export default Card;
