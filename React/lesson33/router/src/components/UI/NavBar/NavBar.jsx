import classes from './NavBar.module.scss';
import { NavLink } from "react-router-dom";

function NavBar() {
  return (
    <ul className={classes.menuBar}>
      <li><NavLink to='/'>Watch Now</NavLink></li>
      <li>Movies</li>
      <li><NavLink to='/tv-shows'>TV Shows</NavLink></li>
      <li>Sports</li>
      <li>Kids</li>
      <li>Library</li>
    </ul>
  );
}

export default NavBar;
