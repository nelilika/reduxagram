import { Navigate } from "react-router-dom";
import { lazy, Suspense } from "react";

const Dashboard = lazy(() => import("../pages/Dashboard/Dashboard"));
const Login = lazy(() => import("../pages/Login/Login"));
const TVShows = lazy(() => import("../pages/TVShows/TVShows"));
const TVShowInfo = lazy(() => import("../pages/TVShowInfo/TVShowInfo"));
const NotFound = lazy(() => import("../pages/NotFound/NotFound"));

function getComponent(Component) {
  const TOKEN = localStorage.getItem("AUTH_TOKEN");
  return TOKEN ? (
    <Suspense>
      <Component />
    </Suspense>
  ) : (
    <Navigate to="/login" />
  );
}

export const routes = [
  {
    path: "/",
    element: getComponent(Dashboard),
  },
  {
    path: "login",
    element: (
      <Suspense>
        <Login />
      </Suspense>
    ),
  },
  {
    path: "tv-shows",
    element: getComponent(TVShows),
  },
  { path: "tv-shows/:id", element: getComponent(TVShowInfo) },
  {
    path: "*",
    element: (
      <Suspense>
        <NotFound />
      </Suspense>
    ),
  },
  // { path: "*", element: <Navigate to='/' /> }
];
