import { useState } from "react";
import { useEffect } from "react";
import NavBar from '../../components/UI/NavBar/NavBar';
import CardList from "../../components/CardList/CardList";
import './TVShows.scss';

function TVShows() {
  const [tvShows, setTvShows] = useState([]);

  useEffect(() => {
    fetch(
      "https://api.themoviedb.org/3/tv/top_rated?api_key=5f06e2a08e0a49e1ebe93ef55540a01c"
    )
      .then((res) => res.json())
      .then((res) => setTvShows(res.results));
  }, []);

  return (
    <div className="tv-shows-wrapper">
      <div className="login-header">
        <div className="name-wrapper">
          <h3>Hello, nelilika</h3>
        </div>
        <button className="custom-btn btn-1">
          <span>Log in</span>
        </button>
      </div>

      <NavBar />
      <CardList cards={tvShows} />
    </div>
  );
}

export default TVShows;
