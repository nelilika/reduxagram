import { useState } from "react";
import { useEffect } from "react";
import NavBar from '../../components/UI/NavBar/NavBar';
import Card from "../../components/Card/Card";
import { useParams, useNavigate } from 'react-router-dom';
import './TVShowInfo.scss';

function TVShowInfo() {
  const [tvShow, setTvShow] = useState({});
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    fetch(
      `https://api.themoviedb.org/3/tv/${id}?api_key=5f06e2a08e0a49e1ebe93ef55540a01c`
    )
      .then((res) => res.json())
      .then((res) => setTvShow(res));
  }, []);

  function navigateBack() {
    navigate('/tv-shows');
  }

  return (
    <div className="tv-show-wrapper">
      <div className="login-header">
        <div className="name-wrapper">
          <h3>Hello, nelilika</h3>
        </div>
        <button className="custom-btn btn-1">
          <span>Log in</span>
        </button>
      </div>

    <NavBar />

    <button onClick={navigateBack}>Back to Shows</button>
      <Card card={tvShow} />
    </div>
  );
}

export default TVShowInfo;
