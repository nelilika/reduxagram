import './Dashboard.scss';
import NavBar from '../../components/UI/NavBar/NavBar';
import { useNavigate } from "react-router-dom";

function Dashboard() {
  let navigate = useNavigate();

  function navigateToLogin() {
    navigate("/login");
  }

  return (
    <div className="dashboard-wrapper">
    <div className="login-header">
      {/* <div className="name-wrapper">
        <h3>Hello, nelilika</h3>
      </div> */}
      <button className="custom-btn btn-1" onClick={navigateToLogin}>
        <span>Log in</span>
      </button>
    </div>

    <NavBar />
  </div>
  );
}

export default Dashboard;
