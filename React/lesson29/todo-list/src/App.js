import { useState, useMemo } from 'react';
import './App.scss';
import TodoList from './components/TodoList/TodoList';
import AddTodo from './components/AddTodo/AddTodo';
import { v4 as uuidv4 } from 'uuid';

function App() {
  const [counter, setCounter] = useState(0);
  const [todos, setTodos] = useState([]);

  // кеширование данних
  const sortedTodos = useMemo(() => {
    return [...todos].sort((todo) => todo.isCompleted ? 1 : -1 );
  }, [todos]);

  // const sortedTodos = [...todos].sort((todo) => todo.isCompleted ? 1 : -1 );

  function addNewTodo(todo) {
    setTodos([todo, ...todos]);
  }

  function increment() {
    setCounter(counter + 1);
  }

  function completeTodo(todoId) {
    const updatedTodos = todos.map(todo => {
      if (todo.id === todoId) {
        return {
          ...todo,
          isCompleted: !todo.isCompleted,
        }
      }
      return todo;
    });
    setTodos(updatedTodos);
  }

  function removeTodo(todoId) {
    const updatedTodos = todos.filter(todo => todo.id !== todoId);
    setTodos(updatedTodos);
  }

  function updateTodo(updatedTodo) {
    const updatedTodos = todos.map(todo => todo.id === updatedTodo.id ? updatedTodo : todo);
    setTodos(updatedTodos);
  }

  return (
    <div className="todo-app">
      <h1 onClick={increment}>What's plan for Today, bro? {counter}</h1>
      <AddTodo addNewTodo={addNewTodo}/>
      <TodoList
        todos={sortedTodos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
      />
    </div>
  );
}

export default App;
