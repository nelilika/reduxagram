import Todo from '../Todo/Todo';

function TodoList({ todos, completeTodo, removeTodo, updateTodo }) {
    return (
        <div className='todo-list'>
            {
                todos.map(todo => (
                    <Todo
                        todo={todo}
                        key={todo.id}
                        completeTodo={completeTodo}
                        removeTodo={removeTodo}
                        updateTodo={updateTodo}
                    />
                ))
            }
        </div>
    );
  }
  
  export default TodoList;