import { combineReducers } from 'redux';
import postReducer from './postsSlice';
import commentsReducer from './commentsSlice';

export default combineReducers({
  posts: postReducer,
  comments: commentsReducer,
});
