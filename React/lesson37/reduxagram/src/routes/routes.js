import { Navigate } from 'react-router-dom';
import Login from '../pages/Login/Login';
import Posts from '../pages/Posts';
import Post from '../pages/Post/Post';
import NotFound from '../pages/NotFound';

function getComponent(Component) {
  const TOKEN = localStorage.getItem('AUTH_TOKEN');
  return TOKEN ? <Component /> : <Navigate to="/login" />;
}

export const routes = [
  {
    path: '/',
    element: getComponent(Posts),
  },
  {
    path: '/post/:id',
    element: getComponent(Post),
  },
  {
    path: 'login',
    element: <Login />,
  },
  {
    path: '*',
    element: <NotFound />,
  },
];
