import React, { useEffect } from "react";
import Button from "@mui/material/Button";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Input } from "../UI/Input";
import SendIcon from "@mui/icons-material/Send";

const schema = yup.object().shape({
  author: yup.string().required("The author is required"),
  comment: yup.string().required("The comment is required"),
});

function AddNewComment({ defaultValues }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(schema),
    defaultValues,
  });

  const onSubmit = (data) => {
    console.log(data);
  };

  useEffect(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  return (
    <form
      noValidate
      style={{ width: "100%", display: "flex" }}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Input
        {...register("comment")}
        id="comment"
        type="text"
        label="Your comment"
        name="comment"
        error={!!errors.comment}
        helperText={errors?.comment?.message}
      />
      <Button
        sx={{
          width: 0,
          height: "55px",
          margin: "16px 0 0 0",
        }}
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
      >
        <SendIcon />
      </Button>
    </form>
  );
}

export default AddNewComment;
