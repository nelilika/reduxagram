import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getComments } from "../../api";

export const fetchCommentsById = createAsyncThunk(
  "comments/fetchCommentsById",
  async (postId) => {
    const { data } = await getComments(postId);
    return data;
  }
);

export const commentsSlice = createSlice({
  name: "comments",
  initialState: {
    comments: [],
    loaded: false,
    loading: false,
    error: null,
  },
  reducers: {
    addComment: (state, action) => {
      console.log("Hello from addComment");
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCommentsById.pending, (state, action) => {
        state.loading = true;
        state.loaded = false;
        state.error = false;
        state.comments = [];
      })
      .addCase(fetchCommentsById.fulfilled, (state, action) => {
        state.loading = false;
        state.loaded = true;
        state.error = null;
        state.comments = action.payload;
      }).addCase(fetchCommentsById.rejected, (state, action) => {
        state.loading = false;
        state.loaded = true;
        state.error = action.error;
      });
  },
});

console.log(commentsSlice);

export const { addComment } = commentsSlice.actions;

export default commentsSlice.reducer;