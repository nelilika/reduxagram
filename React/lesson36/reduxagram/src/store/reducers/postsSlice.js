import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { getPosts, getPost } from "../../api";

export const fetchPosts = createAsyncThunk("posts/fetchPosts", async (params) => {
  const { data: posts, headers } = await getPosts(params);
  const totalCount = +headers['x-total-count'];
  return { posts, totalCount };
});

export const fetchPostById = createAsyncThunk("posts/fetchPost", async (id) => {
  const { data } = await getPost(id);
  return data;
});

const postsSlice = createSlice({
  name: "posts",
  initialState: {
    posts: [],
    selectedPost: {},
    limit: 5,
    page: 1,
    totalCount: 0,
    loaded: false,
    loading: false,
    errors: null,
    loadedPost: false,
    loadingPost: false,
    errorsPost: null,
  },
  reducers: {
    likePost: (state) => {
      /* */
      console.warn("likePost");
      console.log(state);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state, action) => {
        state.loading = true;
        state.loaded = false;
        state.errors = null;
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.posts = action.payload.posts;
        state.selectedPost = action.payload.posts[0];
        state.totalCount = action.payload.totalCount;
        state.loaded = true;
        state.loading = false;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.errors = action.error;
        state.loaded = true;
        state.loading = false;
      })
      .addCase(fetchPostById.pending, (state, action) => {
        state.loadingPost = true;
        state.loadedPost = false;
        state.errorsPost = null;
      })
      .addCase(fetchPostById.fulfilled, (state, action) => {
        state.selectedPost = action.payload;
        state.loadedPost = true;
        state.loadingPost = false;
      })
      .addCase(fetchPostById.rejected, (state, action) => {
        state.errorsPost = action.error;
        state.loadedPost = true;
        state.loadingPost = false;
      });
  },
});

export const { likePost } = postsSlice.actions;

export default postsSlice.reducer;
