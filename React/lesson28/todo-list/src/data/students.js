const studentsName = `Harry Potter
Ron Weasley
Hermione Granger
Colin Creevey
Seamus Finnigan
Hannah Abbott
Pansy Parkinson
Zacharias Smith
Blaise Zabini
Draco Malfoy
Dean Thomas
Millicent Bulstrode
Terry Boot
Ernie Macmillan
Vincent Crabbe
Gregory Goyle
Lavender Brown
Katie Bell
Parvati Patil
Dennis Creevey
Eloise Midgen
Ritchie Coote
Jack Sloper
Victoria Frobisher
Geoffrey Hooper
Andrew Kirke
Demelza Robins
Cormac McLaggen
Neville Longbottom
Ginny Weasley
Romilda Vane
Cho Chang
Marietta Edgecombe
Mandy Brocklehurst
Michael Corner
Stephen Cornfoot
Kevin Entwhistle
Anthony Goldstein
Su Li
Morag McDougal
Padma Patil
Lisa Turpin
Luna Lovegood
Orla Quirke
Stewart Ackerley
Susan Bones
Eleanor Branstone
Kevin Whitby
Tracey Davis
Lilian Moon`;

export const students = studentsName
  .trim()
  .split('\n')
  .map((s, i) => {
    const [firstName, lastName] = s.split(' ');
    return {
      id: i + 1,
      firstName,
      lastName,
      course: getRandomInt(0, 8),
      favoriteLesson: 'Transfiguration',
      isExpelled: getRandomBoolean(),
    };
  });

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomBoolean() {
  return Math.random() > 0.5;
}
