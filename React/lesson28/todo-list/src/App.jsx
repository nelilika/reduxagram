import { Fragment } from 'react';
import './App.css';
import Students from './Students';

function App() {
  return (
    <Fragment>
      <Students />
    </Fragment>
  );
}

export default App;
