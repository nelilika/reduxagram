import { useState } from 'react';
import { students as studentsData } from './data/students';
import Student from './Student';

export default function Students() {
  const [students, setStudents] = useState(studentsData);

  if (!students.length) {
    return <h1>No students found</h1>;
  }

  function removeStudent(id) {
    const newStudents = students.filter((s) => s.id !== id);
    setStudents(newStudents);
  }

  return students.map((student) => (
    <Student student={student} removeStudent={removeStudent} key={student.id} />
  ));
}
