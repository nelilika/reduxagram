import { Fragment, useState } from 'react';

function Counter() {
  let [counter, setCounter] = useState(10);

  function decrement() {
    setCounter(--counter);
  }

  function increment() {
    setCounter(++counter);
  }

  return (
    <Fragment>
      <h1>Counter: {counter}</h1>
      <button className="btn" onClick={decrement}>
        Decrement
      </button>
      <button className="btn" onClick={increment}>
        Increment
      </button>
    </Fragment>
  );

  // или
  // return (
  //   <>
  //     <button className="btn">Click me</button>
  //     <button className="btn">Click me</button>
  //   </>
  // );
}

export default Counter;
