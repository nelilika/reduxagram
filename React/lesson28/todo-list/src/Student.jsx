import React from 'react';

export default function Student({ student, removeStudent }) {
  return (
    <div
      style={{
        border: `3px solid ${student.isExpelled ? 'red' : 'green'}`,
      }}
      className="student"
      key={student.id}
    >
      <span
        style={{
          float: 'right',
          cursor: 'pointer',
        }}
        onClick={() => removeStudent(student.id)}
      >
        x
      </span>
      <h3>
        {student.firstName} {student.lastName}
      </h3>
      <p>{student.favoriteLesson}</p>
    </div>
  );
}
