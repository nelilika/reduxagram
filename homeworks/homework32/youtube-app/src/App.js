import { Component } from "react";
import "./App.css";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import SearchIcon from '@mui/icons-material/Search';

const itemData = [
  {
    img: "https://images.unsplash.com/photo-1551782450-a2132b4ba21d",
    title: "Burger",
    author: "@rollelflex_graphy726",
  },
  {
    img: "https://images.unsplash.com/photo-1522770179533-24471fcdba45",
    title: "Camera",
    author: "@helloimnik",
  },
  {
    img: "https://images.unsplash.com/photo-1444418776041-9c7e33cc5a9c",
    title: "Coffee",
    author: "@nolanissac",
    cols: 2,
  },
  {
    img: "https://images.unsplash.com/photo-1533827432537-70133748f5c8",
    title: "Hats",
    author: "@hjrc33",
    cols: 2,
  },
  {
    img: "https://images.unsplash.com/photo-1558642452-9d2a7deb7f62",
    title: "Honey",
    author: "@arwinneil",
    rows: 2,
    cols: 2,
    featured: true,
  },
];

export default class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <Box sx={{ flexGrow: 1,  padding: '10px', }}>
        <Grid container spacing={2}>
          <Grid item md={8} xs={12}>
            <form style={{ width: '100%' }}>
              <TextField id="outlined-basic" label="Search your videos..." variant="outlined" sx={{ width: '90%' }} />
              <Button variant="contained" sx={{ height: '55px', borderRadius: '0 5px 5px 0', width: '10%' }}> <SearchIcon /></Button>
            </form>
            <div style={{ paddingTop: '30px', height: '80vh' }}>
              <iframe
                width="100%"
                height="100%"
                src="https://www.youtube.com/embed/GdpObUntCCM"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
            <h4>Some Description to the video</h4>
          </Grid>
          <Grid item md={4} xs={12}>
            <h3 style={{ marginBottom: '42px' }}>Suggested videos:</h3>
            <ImageList
              sx={{
                width: "100%",
                height: "77vh",
                display: "flex",
                flexDirection: "column",
              }}
            >
              {itemData.map((item) => (
                <ImageListItem key={item.img}>
                  <img
                    src={`${item.img}?w=248&fit=crop&auto=format`}
                    srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                    alt={item.title}
                    loading="lazy"
                    style={{ height: "146px" }}
                  />
                  <ImageListItemBar
                    title={item.title}
                    subtitle={item.author}
                    actionIcon={
                      <IconButton
                        sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                        aria-label={`info about ${item.title}`}
                      >
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </ImageListItem>
              ))}
            </ImageList>
          </Grid>
        </Grid>
      </Box>
    );
  }
}
