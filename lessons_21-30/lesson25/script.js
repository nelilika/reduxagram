// String
// Number
// Boolean
// undefined
// null
// Object
// BigInt

// Symbol - примитивный тип данных
// создает уникальный идентификатор (ключ)

const id1 = Symbol('studentId');
const id2 = Symbol('studentId');
console.log(id1 === id2); // false
console.log(id1 === id2); // false

const student = {
  name: 'Agata',
  age: 25,
  country: 'Ukraine',
  [id1]: 1, // Symbol('studentId')
};

console.log(student.id);
student.id = 4;
console.log(student.id);

// Map и Set
// const obj = {
//   key: value,
// };
// key - встегда строка
// value - может быть любой тип данных

// Map - коллекция данных, где ключ может быть любым типом данных

const map = new Map();

map.set(null, 'null'); // установить пару ключ-значение
map.set(true, 'boolean');
map.set(1, 'number');
map.set(undefined, 'undefined');

for (let value of map.keys()) {
} // перебор ключей Map
for (let value of map.values()) {
} // перебор значений Map
for (let value of map) {
} // перебор данных Map -> возвращает массив [ключ, значение]

// Set - коллекция данных, которая содержит только уникальные значения
const set = new Set();

const size = 10;
let i = 0;

while (set.size < 10) {
  set.add(getRandomInt(1, 11));
  i++;
}
console.log(set);
console.log(i);

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}

// const arr = [1,2,3,4,5,6,7,8,9,10];
// console.time('For: ');
// const arr1 = [];
// for (let i = 0; i < 10000; i++) {
//   arr1.push(i);
// }
// console.timeEnd('For: ');

// console.time('Methods: ');
// const arr2 = new Array(10000).fill(null).map((item, i) => i);
// console.timeEnd('Methods: ');

const arr = [];
const setArr = new Set();
for (let i = 0; i < 1e5; i++) {
  arr.push(i);
  setArr.add(i);
}

// найти элемент из массива
let value = 99676;
let isExist = false;
console.time('For');
for (let i = 0; i < arr.length; i++) {
  if (i === value) {
    isExist = true;
    break;
  }
}
console.timeEnd('For');

console.time('Methods');
const isExistInArr = arr.find((item, i) => i === value);
console.timeEnd('Methods');

console.time('Set');
const isExistInSet = setArr.has(value);
console.timeEnd('Set');

// удалить элемент из массива
console.time('Methods');
const index = arr.findIndex((item, i) => i === value);
arr.splice(index, 1);
console.timeEnd('Methods');

console.time('Set');
setArr.delete(value);
console.timeEnd('Set');

// Итераторы
// for ... of

// что можно перебирать ?
// - массивы
// - строки
// - Map
// - Set
// ...объекты
// ...функции

// new Array(872).fill(null).map((item, i) => i);

const obj = {
  min: 1,
  max: 872,
};

const iterationObj = {
  min: 1,
  max: 8,
  [Symbol.iterator]() {
    return {
      current: this.min,
      last: this.max,
      next() {
        if (this.current < this.last) {
          return { done: false, value: this.current++ };
        }
        return { done: true, value: this.current++ };
      },
    };
  },
};

const getRandomValues = {
  min: 1,
  max: 11,
  amount: 10,
  [Symbol.iterator]() {
    let index = 0;
    return {
      min: this.min,
      max: this.max,
      amount: this.amount,
      next() {
        if (index < this.amount) {
          index++;
          return { done: false, value: getRandomInt(this.min, this.max) };
        }
        return { done: true, value: getRandomInt(this.min, this.max) };
      },
    };
  },
};

Array.from(getRandomValues);

// Генераторы

function* getNumber() {
  console.log('Start');
  let a = 1;
  let b = 15;
  yield a;
  console.log('Calculate c');

  let c = a + b;

  yield b;

  // return 'Some value';

  yield c;
  console.log('Finish');
}

const generateNumber = getNumber();
console.log(generateNumber);

for (let value of generateNumber) {
  console.log('Value', value);
}
