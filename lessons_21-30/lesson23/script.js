// Fetch


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    auth: 'Hello Im custom header',
  },
  body: JSON.stringify({
    id: '1224',
    text: '234',
  }), // тип данных в body всегда текст
}).then(data => data.json())
.then(data => {
  console.log(data);
});

fetch('https://jsonplaceholder.typicode.com/todos?_limit=4&_page=3')
.then(data => {
  console.log(data.headers);
  console.log(data.headers.get('x-total-count'));
  return data;
})
.then(data => {
  console.log(data);
});

// async / await
// try / catch / finally
// возможность работать синхронно с асинхронным кодом

async function getTodoById(id) {
  if (id == 88) {
    const rawData = await fetch(`https://jsonplaceholder.typicode.com/todo/${id}`);
    return rawData.json();
  }

  const rawData = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`);
  // console.log(rawData);
  return rawData.json();
}

// new Promise((res, rej) => {
//   function getTodoById(id) {
//     const rawData = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`);
//     console.log(rawData);
//     return rawData.json();
//   }
//   res(getTodoById(id));
// })



async function start() {
  try {
    // асинхронный код.
    const data4 = await getTodoById(4); // Promise.resolve().then();
    console.log(data4);

    const data88 = await getTodoById(88); // Promise.resolve().then();
    console.log(data88);

    const data12 = await getTodoById(12); // Promise.resolve().then();
    console.log(data12);
  } catch(err) {
    console.error(err);
  } finally {
    console.warn('Finally');
  }
}

start();