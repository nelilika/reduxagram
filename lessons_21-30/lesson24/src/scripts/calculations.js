import { Aside, Bside } from './variables';


export default function getPerimeter() {
  return Aside - Bside;
}