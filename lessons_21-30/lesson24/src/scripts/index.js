import '../styles/style.scss';
import getPerimeter from './calculations';

console.log('Hello Webpack!');

const student = {
  name: 'Alf',
  country: 'Alfacentavra',
}

const student1 = {
  ...student,
  country: 'Ukraine',
}

console.log(student, student1);
console.log(getPerimeter());