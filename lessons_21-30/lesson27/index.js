// console.log('Hello world from NodeJs');

// const a = 1;
// const b = 3;
// console.log(a + b);

// module.exports = {
//     a, b
// };

const express = require('express');
const { empolyee } = require('./employees');
const util = require('util');
const fs = require('fs');
const { join } = require('path');
var bodyParser = require('body-parser')
const app = express();
const PORT = 3000;

const ERROR_CODE_400 = 400;
const ERROR_CODE_419 = 419;
const ERROR_CODE_500 = 500;

const readFileAsPromise = util.promisify(fs.readFile)
const writeFileAsPromise = util.promisify(fs.writeFile)

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: true
}));

// parse application/json
app.use(bodyParser.json())
app.use(error);

app.get('/hello', (req, res) => {
  res.send('Hello World!');
  // throw new Error('400');
});

app.get('/users_import', (req, res) => {
    // const users = [
    //     {
    //         id: 1,
    //         name: 'Ola',
    //         surname: 'Ola',
    //         age: 25,
    //     },
    //     {
    //         id: 2,
    //         name: 'Ola',
    //         surname: 'Ola',
    //         age: 25,
    //     },
    //     {
    //         id: 2,
    //         name: 'Ola',
    //         surname: 'Ola',
    //         age: 25,
    //     },
    // ]
    res.send(empolyee);
  });

app.get('/users_fs', async (req, res) => {
    /* Синхронное получение файла - -1 / 10 */
    // const data = fs.readFileSync('./README.md', 'utf-8');

    /* АСИНХРОННОЕ получение файла, через функцию колбек 6.5 / 10 */
    // fs.readFile('./employes.json', 'utf-8', (err, data) => {
    //   if (err) throw new Error(ERROR_CODE_400);
    //   res.send(data);
    // });

    /* АСИНХРОННОЕ получение файла, через промис 8 / 10 */
    // readFileAsPromise('./employes.json', 'utf-8')
    //     .then(data => res.send(data));

    /* АСИНХРОННОЕ получение файла, через промис 10 / 10 */
    const data = await readFileAsPromise('./employes.json', 'utf-8');
    res.send(data);
});

app.post('/users_fs/:id', async (req, res) => {
    // req.body - обїект которий приходит из клиента
    /* Синхронное получение файла - -1 / 10 */
    // const data = fs.readFileSync('./README.md', 'utf-8');

    /* АСИНХРОННОЕ получение файла, через функцию колбек 6.5 / 10 */
    // fs.readFile('./employes.json', 'utf-8', (err, data) => {
    //   if (err) throw new Error(ERROR_CODE_400);
    //   res.send(data);
    // });

    /* АСИНХРОННОЕ получение файла, через промис 8 / 10 */
    // readFileAsPromise('./employes.json', 'utf-8')
    //     .then(data => res.send(data));

    /* АСИНХРОННОЕ запись файла, через промис 10 / 10 */
    console.log(req.body);
    await writeFileAsPromise('./random.json', JSON.stringify(req.body));
    res.status(201);
    res.send('Success');
});

app.post('/users/:id', (req, res) => {
    if (req.params.id === '2345') {
        throw new Error(ERROR_CODE_400);
    } else if (req.params.id === '5432') {
        throw new Error(ERROR_CODE_419);
    }

    res.status(403);
    res.send(`Got a POST request, ${req.params.id}`);
})



app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`)
})


function error(err, req, res, next) {
    if (err.message === ERROR_CODE_400) {
        res.status(ERROR_CODE_400);
        res.send('Bad Request');
    }

    res.status(ERROR_CODE_500);
    res.send('Internal Server Error');
  }