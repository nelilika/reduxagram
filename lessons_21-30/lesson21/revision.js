/* Initial song list */
let songs = [
  {
    name: 'Jingle Bells',
    isLiked: false,
  },
  {
    name: 'We Wish You a Merry Christmas',
    isLiked: true,
  },
];

// Лайкает все песни в массиве

// songs.map((song) => song.isLiked = true);
// songs[0].isLiked = true;
// songs[1].isLiked = true;

// const likedSongs = songs.map((song) => {
//   return { ...song, isLiked: true };
// });

const likedSongs = songs.map((song) => ({ ...song, isLiked: true }));

// Вывод на экран только лайкнутых песен

/* songs.filter(song => {
  if (song.isLiked) {
    console.log(song);
  } 
}) */

const filteredSongs = songs.filter((song) => song.isLiked);
for (let song of filteredSongs) {
  console.log(song);
}
