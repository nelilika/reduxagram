/* Promises */

// Пример работы с асинхронностью через функции колбек
function getServerResponce(params, callbackFn) {
  console.log('started getting data from server');
  setTimeout(() => {
    const student = {
      id: '1234',
      name: 'Vitek',
      age: 25,
    };
    if (student.id === params) {
      callbackFn(student);
    }
  }, 2500);
}

const studentId = '1234';

// getServerResponce(studentId, callbackFn);

function callbackFn(response) {
  console.log('got data from server');
  console.log(response);
}

// Пример работы с асинхронностью через промисы
const promise = new Promise(function (resolve, reject) {
  setTimeout(() => {
    const student = {
      id: '1234',
      name: 'Vitek',
      age: 25,
    };
    if (studentId === student.id) {
      resolve(student);
    } else {
      reject('Sorry, student not found');
    }
  }, 2500);
});

// console.log(promise);

// Promise.prototype.then()
// then ловит данные которые отправляются промисом с помощью функции resolve
promise.then((response) => {
  // console.log(response);
});

// Promise.prototype.catch()
// catch ловит данные которые отправляются промисом с помощью функции reject
promise.catch((error) => {
  // console.error(error);
});

// Promise.prototype.finally()
// finally ловит любые данные
promise.finally(() => {
  // console.warn('Im in the finally place');
});

// цепочка промисов
// promise.then().then().catch().then().then().catch().finally();

function generatePromise(message, time, isSuccess) {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      if (isSuccess) {
        resolve(message);
      } else {
        reject(message);
      }
    }, time);
  });
}

// каркас - базовая функция по созданию простейшего промиса
function generateCommonPromise() {
  return new Promise(function (resolve, reject) {
    resolve(); // reject()
  });
}

// Статические методы промисов

// Promise.all()
// массив всех УСПЕШНЫХ промисов или же ПЕРВЫЙ отклоненный (с ошибкой)

console.time('Promise.all');
Promise.all([
  generatePromise('First promise', 500, false),
  generatePromise('Second promise', 1500, true),
  generatePromise('Third promise', 1000, true),
])
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.error(error);
  })
  .finally(() => {
    console.timeEnd('Promise.all');
  });

// Promise.

// Promise.race()
// возвращает ЛЮБОЕ значение первого (самого быстрого) промиса

console.time('Promise.race');
Promise.race([
  generatePromise('First promise', 600, false),
  generatePromise('Second promise', 1500, false),
  generatePromise('Third promise', 1000, false),
])
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.error(error);
  })
  .finally(() => {
    console.timeEnd('Promise.race');
  });

// Promise.resolve();
// возвращает успешный промис всегда

// new Promise(function (resolve, reject) {
//   resolve('Some success message');
// }).then((res) => console.log(res));

Promise.resolve('Some success message').then((res) => console.log(res));

// Promise.reject();
// возвращает отклоненный (с ошибкой) промис всегда

// new Promise(function (resolve, reject) {
//   reject('Some rejected message');
// }).catch((res) => console.error(res));

Promise.reject('Some rejected message').catch((res) => console.error(res));

// Опционально
// Promise.allSettled()
Promise.allSettled;

const p1 = Promise.resolve()
  .then(() => console.log(1))
  .then(() => console.log(2))
  .then(() => console.log(3));

const p2 = Promise.resolve()
  .then(() => console.log(4))
  .then(() => console.log(5))
  .then(() => console.log(6));

Promise.all([p1, p2]).then((res) => console.log(res)); // 1 4 2 5 3 6


new Promise(() => {
  resolve();
  reject();
})

Promise.resolve();

Promise.reject();

Promise.resolve(1)
.then(res => console.log(2))
.then(res => Promise.reject(3))
.then(res => console.log(4))
.then(res => console.log(5))
.catch(res => console.log(6))
.then(res => console.log(7)); // 2 6 7


// Promise.resolve('')
//   .then((res) => { // 'string'
//     if (!res) {
//       return getPromise();
//     }
//       return Promise.resolve();
//   })
//   .then(res => {
//     return 3;
//   })
//   .then()
//   .catch(err => {
//     throw new Error(err)
//   });


// function getPromise() {
//   return new Promise((resolve) => {
//     resolve('hi')
//   });
// }